<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 27.03.2017
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="locale.jsp" />
<fmt:bundle basename="pagecontext" prefix = "label." >
    <html>
        <head>
            <title>Create question</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/askQuestion.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
            <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        </head>
        <body>
            <main>
                <jsp:include page="header.jsp" />
                <jsp:include page="avatar.jsp" />
                <div class="form-container">
                    <div  id="error-info" class="error-create-question">
                        <span class="text-info">
                            <fmt:message key="ask.question.restriction" />
                        </span>
                    </div>
                    <form onsubmit="return checkForm(this)" action="/controller?command=new_question" method="post">
                        <label for="question-form-title" class="token">
                            <fmt:message key="header" />
                        </label>
                        <input type="text" id="question-form-title" name="header"
                               placeholder="<fmt:message key="what.question" />">
                        <textarea id="question-form-body" maxlength="1000" name="question-body"></textarea>
                        <label class="token" for="question-form-topics">
                            <fmt:message key="markers" />
                        </label>
                        <input type="text" id="question-form-topics" name="tokens"
                               placeholder="<fmt:message key="markers.restriction" />">
                        <input class="form-submit" type="submit" value="<fmt:message key="submit" />"
                               id="ask-question-submit" disabled>
                    </form>
                </div>
            </main>
            <jsp:include page="footer.jsp" />
            <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
            <script src="${pageContext.request.contextPath}/page/resources/js/askQuestion.js"></script>
            <script>
                checkAuthorization('${user}');
            </script>
        </body>
    </html>
</fmt:bundle>