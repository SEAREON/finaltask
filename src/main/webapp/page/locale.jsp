<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 19.05.2017
  Time: 0:29
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:choose>
    <c:when test="${locale == 'en'}">
        <fmt:setLocale value="en" scope="session" />
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="ru" scope="session" />
    </c:otherwise>
</c:choose>
