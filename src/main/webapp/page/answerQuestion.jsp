<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 08.04.2017
  Time: 21:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<jsp:include page="locale.jsp" />
<fmt:bundle basename="pagecontext" prefix = "label." >
    <html>
    <head>
        <title>Answer question</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/answerQuestion.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/new.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/delete.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="header.jsp" />
    <jsp:include page="avatar.jsp" />
    <main>
        <div class="content clearfix">
            <div class="mainbar">
                <div class="question-answers">
                    <div class="question">
                            <%--get question title--%>
                        <div id="div-question">
                            <h1 id="question-title">
                                ${object.title}
                                <ctg:state-of-entity role="${user.role}" newEntity="${object.newEntity}"
                                                     active="${object.active}" />
                            </h1>
                            <%--get question text--%>
                            <p id="question-body">${object.text}</p>
                            <div class="tag" id="question-topics">
                                <c:forEach items="${topics}" var="topic">
                                    <a href="/controller?command=questions_by_topic&id=${topic.id}"
                                       class="post-tag">
                                            ${topic.text}
                                    </a>
                                </c:forEach>
                            </div>
                        </div>
                        <form onsubmit="return checkForm(this)" action="/controller" id="question-form"
                              class="form-container" method="post">
                            <input type="hidden" name="command" value="update_question">
                            <input type="hidden" name="type" value="question">
                            <input type="hidden" name="id" value="${object.id}">
                            <input type="hidden" name="oldTopics" id="question-form-old-topics">
                            <label for="question-form-title" class="token">
                                <fmt:message key="header" />
                            </label>
                            <input type="text" id="question-form-title" name="header"
                                   placeholder="<fmt:message key="what.question" />" value="${object.title}">
                            <textarea maxlength="1000" id="question-form-body" name="question-body">${object.text}</textarea>
                            <label class="token" for="question-form-topics">
                                <fmt:message key="markers" />
                            </label>
                            <input type="text" id="question-form-topics" name="tokens"
                                   placeholder="<fmt:message key="markers.restriction" />">
                            <input class="form-submit" type="submit" value="<fmt:message key="save" />">
                        </form>
                        <ctg:date-who validate="true" object="${object}" time="${object.time}" accounts="${accounts}"
                                      edit="true"/>
                    </div>
                    <div class="answers">
                        <%--forEach for answers--%>
                        <c:forEach items="${list1}" var="answer" varStatus="index">
                            <c:if test="${answer.active == true || user.role == true}">
                                <div class="answer">
                                    <table id="table-${index.count}">
                                        <tbody>
                                        <tr>
                                            <td id="rating-${answer.hashCode()}" class="answer-rating-td">
                                                <div class="center">
                                                    <ctg:state-of-entity role="${user.role}"
                                                                         newEntity="${answer.newEntity}"
                                                                         active="${answer.active}" />
                                                </div>
                                                <div class="answer-rating-div">
                                                    <c:if test="${answer.active == true}">
                                                        <a id="up-${index.count}"
                                                           href="/controller?command=rating&action=up&id=${answer.id}"
                                                           class="answer-rating-up-off"
                                                           title="<fmt:message key="useful" />">
                                                            <fmt:message key="vote.for" />
                                                        </a>
                                                    </c:if>
                                                    <span id="mark-${index.count}" class="rating-value">
                                                            ${answer.mark}
                                                    </span>
                                                    <c:if test="${answer.active == true}">
                                                        <a id="down-${index.count}"
                                                           href="/controller?command=rating&action=down&id=${answer.id}"
                                                           class="answer-rating-down-off"
                                                           title="<fmt:message key="not.benefit" />">
                                                            <fmt:message key="against" />
                                                        </a>
                                                    </c:if>
                                                </div>
                                            </td>
                                            <td class="answer-text-td">
                                                <div class="answer-text-div" id="answer-${answer.hashCode()}">
                                                        ${answer.text}
                                                </div>
                                                <form onsubmit="return checkAnswerForm(this, ${answer.hashCode()})"
                                                      action="/controller" id="answer-form-${answer.hashCode()}"
                                                      class="form-container" method="post">
                                                    <input type="hidden" name="command"
                                                           value="update_answer">
                                                    <input type="hidden" name="id" value="${answer.id}">
                                                    <textarea maxlength="1000" id="answer-form-body-${answer.hashCode()}"
                                                              name="answer-body">${answer.text}</textarea>
                                                    <input class="form-submit" type="submit"
                                                           value="<fmt:message key="save" />">
                                                </form>
                                                <ctg:date-who validate="true" object="${answer}" time="${answer.time}"
                                                              accounts="${accounts}" edit="true"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </div>
                <div class="similar-topic-menu" style="float: left">
                    <h4 class="h-related"><fmt:message key="similar" /></h4>
                    <div class="related-questions">
                            <%--forEach for likeQuestions--%>
                        <c:forEach items="${list2}" var="likeQuestion">
                            <div class="related-question">
                                <div class="count-answer-in-similar-question">
                                        ${likeQuestion.countAnswers}
                                </div>
                                <a href="/controller?command=answer_question&id=${likeQuestion.id}"
                                   class="question-hyperlinc">
                                        ${likeQuestion.title}
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
            <%--check question active--%>
        <c:if test="${object.active == true && not empty user}">
            <form method="post" action="/controller" class="post-form">
                <input type="hidden" name="command" value="answer">
                    <%--send question id in hidden field--%>
                <input type="hidden" name="question-id" value="${object.id}">
                <h2><fmt:message key="your.answer" /></h2>
                <div>
                    <div class="info-my-answer">
                        <fmt:message key="include.code" /> &#60;code&#62;&#60;/code&#62;
                    </div>
                    <div class="info-my-answer">
                        <fmt:message key="make.paragraph" /> &#60;p&#62;&#60;/p&#62;
                    </div>
                </div>
                <textarea name="post-text" id="post-text"></textarea>
                <input type="submit" value="<fmt:message key="send.answer" />" onclick="prepareAnswer();">
            </form>
        </c:if>
    </main>
    <jsp:include page="footer.jsp" />
    <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
    <script src="${pageContext.request.contextPath}/page/resources/js/answerQuestion.js"></script>
    <script src="${pageContext.request.contextPath}/page/resources/js/askQuestion.js"></script>
    <script src="${pageContext.request.contextPath}/page/resources/js/editQuestionAnswers.js"></script>
    <script src="${pageContext.request.contextPath}/page/resources/js/deleteAnswer.js"></script>
    <script src="${pageContext.request.contextPath}/page/resources/js/query.js"></script>
    </body>
    </html>
</fmt:bundle>