<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 07.04.2017
  Time: 16:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<jsp:include page="locale.jsp" />
<fmt:bundle basename="pagecontext" prefix = "label." >
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>Like IT</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/question_body.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/new.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/delete.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/pages.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    </head>
    <body>
        <jsp:include page="header.jsp" />
        <jsp:include page="avatar.jsp" />
        <main>
            <div class="content clearfix">
                <div class="mainbar">
                    <%--forEach for questions--%>
                    <c:forEach items="${list1}" var="question">
                        <div class="question">
                            <div class="count-answers">
                                <div class="counter">
                                    <span>${question.countAnswers}</span>
                                </div>
                                <div><fmt:message key="quantity" /></div>
                                <div class="div-center"><fmt:message key="answers" /></div>
                            </div>
                            <div class="summary">
                                <h3>
                                    <a href="/controller?command=answer_question&id=${question.id}" class="link">
                                        ${question.title}
                                    </a>
                                    <ctg:state-of-entity role="${user.role}" newEntity="${question.newEntity}"
                                                         active="${question.active}" newAnswers="${question.newAnswers}"
                                                         deleteAnswers="${question.deleteAnswers}" />
                                </h3>
                                <div class="tag">
                                    <%--forEach for TopicQuestions--%>
                                    <c:forEach items="${list2}" var="topicQuestion">
                                        <c:forEach var="entry" items="${topicQuestion.id}">
                                            <c:if test="${entry.key == question.id}">
                                                <c:forEach items="${topics}" var="topic">
                                                    <c:if test="${entry.value == topic.id}">
                                                        <a href="/controller?command=questions_by_topic&id=${topic.id}"
                                                           class="post-tag">
                                                                ${topic.text}
                                                        </a>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>
                                </div>
                                <ctg:date-who object="${question}" time="${question.time}" accounts="${accounts}" />
                            </div>
                        </div>
                    </c:forEach>
                    <div class="topic-menu clearfix">
                        <h4 class="topic-title">
                            <fmt:message key="topics" />
                        </h4>
                        <div id="topic-list">
                            <c:forEach items="${topicMenu}" var="topic">
                                <div class="tag">
                                    <a href="/controller?command=questions_by_topic&id=${topic.id}" class="post-tag">
                                        ${topic.text}
                                    </a>
                                </div>
                            </c:forEach>
                            <p class="ar">
                                <a href="/controller?command=topics&currentPage=1">
                                    <fmt:message key="all_topics" />
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="pages.jsp" />
        </main>
        <jsp:include page="footer.jsp" />
        <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
    </body>
</html>
</fmt:bundle>