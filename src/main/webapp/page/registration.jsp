<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 21.03.2017
  Time: 22:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="locale.jsp" />
<fmt:bundle basename="pagecontext" prefix = "label." >
    <html>
        <head>
            <title>Registration</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/registration_body.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
            <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        </head>
        <body>
            <jsp:include page="header.jsp" />
            <jsp:include page="avatar.jsp" />
            <main>
                <div class="info-login info" id="message-info-login">
                    <span class="text-info">
                        <fmt:message key="login.restrictions" />
                    </span>
                </div>
                <div class="info-password info" id="message-info-password">
                    <span class="text-info">
                        <fmt:message key="password.restrictions" />
                    </span>
                </div>
                <div class="error-image " id="message-error-image">
                    <span class="text-info">
                        <fmt:message key="max.size" />
                    </span>
                </div>
                <div class="form-container">
                    <form action="/controller?command=registration" method="post" enctype="multipart/form-data">
                        <label for="display-login">
                            <fmt:message key="login" />
                            <span class="comment">
                                <fmt:message key="obligatory.field" />
                            </span>
                        </label>
                        <br>
                        <input type="text" class="input-required" name="display-login" id="display-login"
                               onfocus="infoFocus('message-info-login')" onblur="infoBlur('message-info-login')"
                               placeholder="Veniamin_13" pattern="[A-z]{1}([A-z]|[0-9]|_){4,}" required>
                        <br>
                        <label for="display-password">
                            <fmt:message key="password" />
                            <span class="comment">
                                <fmt:message key="obligatory.field" />
                            </span>
                        </label>
                        <br>
                        <input type="password" class="input-required" name="display-password" id="display-password"
                               onfocus="infoFocus('message-info-password')" onblur="infoBlur('message-info-password')"
                               placeholder="******" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                               required>
                        <br>
                        <label for="email">
                            Email
                            <span class="comment">
                                <fmt:message key="obligatory.field" />
                            </span>
                        </label>
                        <br>
                        <input type="text" class="input-required" name="email" id="email" placeholder="you@example.org"
                               pattern="^(\w|\d|-|_)+@\w*\.\w+$" required>
                        <br>
                        <label for="description"><fmt:message key="about.myself" /></label>
                        <br>
                        <textarea id="description" maxlength="1000" name="description"></textarea>
                        <br>
                        <label for="upload"><fmt:message key="avatar" /></label>
                        <br>
                        <div class="file-form">
                            <div id="file-form-label"></div>
                            <div class="select-button"><fmt:message key="overview" /></div>
                            <input type="file" name="upload" id="upload" onchange="getName(this.value), checkSize();"
                                   accept="image/*, image/jpeg">
                        </div>
                        <br>
                        <input type="submit" class="sub" value="<fmt:message key="registration" />"
                               onclick="preSubmit()">
                    </form>
                </div>
            </main>
            <jsp:include page="footer.jsp" />
            <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
            <script src="${pageContext.request.contextPath}/page/resources/js/registration.js"></script>
        </body>
    </html>
</fmt:bundle>