<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 03.04.2017
  Time: 0:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<jsp:include page="locale.jsp" />
<fmt:bundle basename="pagecontext" prefix = "label." >
    <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <title>Account info</title>
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/editAccount.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/new.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/delete.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
            <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        </head>
        <body>
            <jsp:include page="header.jsp" />
            <jsp:include page="avatar.jsp" />
            <main>
                <hr>
                <form action="/controller" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="command" value="save_changes_account">
                    <%--hidden field with userEdit login--%>
                    <input type="hidden" name="login-edit-account" value="${userEdit.login}">
                        <%--hidden field with userEdit password--%>
                    <input type="hidden" name="password-edit-account" value="${userEdit.password}">
                    <div class="user-card">
                        <div class="user-card-avatar-wrapper">
                            <div class="user-card-avatar">
                            <c:choose>
                                <c:when test="${userEdit.image != 0}">        <%--error??!--%>
                                    <%--img with userEdit image--%>
                                    <img id="user-ava" src="/controller?command=read_image&image=${userEdit.image}"
                                        alt="ava" width="180" height="220">
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/page/image/anonymous_avatar.png"
                                         width="180" height="220" alt="ava">
                                </c:otherwise>
                            </c:choose>
                            </div>
                            <div id="file-form" class="file-form">
                                <div id="file-form-label"></div>
                                <div class="select-button"><fmt:message key="new" /></div>
                                <input type="file" name="upload" id="upload"
                                       onchange="getName(this.value), checkSize();" accept="image/*, image/jpeg">
                            </div>
                            <%--if userEdit image != 0--%>
                            <c:if test="${userEdit.image != 0}">
                                <%--onclick delete ava userEdit--%>
                                <input type="button" value="<fmt:message key="delete.photo" />" id="delete-user-ava"
                                       onclick="deleteAva(${userEdit.image})">
                            </c:if>
                        </div>
                        <div class="about">
                            <%--userEdit login--%>
                            <h2 class="user-card-name">
                                <span id="user-card-name-id">${userEdit.login}</span>
                                <c:if test="${user.role == true}">
                                    <c:choose>
                                        <c:when test="${userEdit.newEntity == true && userEdit.active == true}">
                                            <span id="new" class="new new-question">new</span>
                                        </c:when>
                                        <c:when test="${userEdit.active == false}">
                                            <span id="new" class="delete new-question">delete</span>
                                        </c:when>
                                    </c:choose>
                                </c:if>
                            </h2>
                            <input type="text" class="user-card-name" id="user-card-name-edit-id"
                                   name="login-edit" value="${userEdit.login}">
                            <div class="discription" id="div-discription">
                                <%--userEdit description--%>
                                <p>${userEdit.description}</p>
                            </div>
                            <textarea id="area-description" name="area-description">${userEdit.description}</textarea>
                        </div>
                        <div class="info">
                            <div class="info-elem">
                                <fmt:message key="questions" />:
                                <%--userEdit count of questions--%>
                                <span class="number">${userEdit.countOfQuestions}</span>
                            </div>
                            <div class="info-elem">
                                <fmt:message key="Answers" />:
                                <%--userEdit count of answers--%>
                                <span class="number">${userEdit.countOfAnswers}</span>
                            </div>
                            <div class="info-elem">
                                <fmt:message key="rating" />:
                                <%--userEdit rating--%>
                                <span class="number">${userEdit.rating}</span>
                            </div>
                            <div class="info-elem">
                                Email:
                                <br>
                                <%--userEdit email--%>
                                <span id="email-user" class="email-element">${userEdit.email}</span>
                                <input type="text" name="email-user-edit" id="email-user-edit"
                                       value="${userEdit.email}">
                            </div>
                            <c:if test="${user.role == true}">
                                <div class="info-elem">
                                    Role:
                                    <c:choose>
                                        <c:when test="${userEdit.role == true}">
                                            <span class="role">Admin</span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="role">User</span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <c:if test="${userEdit.active == true}">
                                    <div class="change-role">
                                        <a id="change-role-a" onclick="queryToServer('/controller?command=change_user_role&id=${userEdit.id}',
                                                '/controller?command=edit&userId=${userEdit.id}',
                                                'Error during change account role. Try again.')">
                                            change role
                                        </a>
                                    </div>
                                </c:if>
                            </c:if>
                        </div>
                        <c:if test="${user.role == true || user.id == userEdit.id}">
                            <div class="right">
                                <c:if test="${userEdit.newEntity == true && userEdit.active == true && user.role == true}">
                                    <a id="verified" onclick="queryToServer('/controller?command=account_verified&id=${userEdit.id}',
                                            '/controller?command=edit&userId=${userEdit.id}',
                                            'Error during validate account. Try again.')">
                                        validate
                                    </a>
                                </c:if>
                                <c:choose>
                                    <c:when test="${userEdit.active == true}">
                                        <a class="edit-image edit-delete" title="<fmt:message key="delete"/>"
                                           onclick="queryToServer('/controller?command=delete&entity=account&id=${userEdit.id}',
                                                   '/controller?command=edit&userId=${userEdit.id}',
                                                   'Error during delete account. Try again.')"></a>
                                    </c:when>
                                    <c:otherwise>
                                        <a class="edit-image resurrect" title="<fmt:message key="resurrect" />"
                                           onclick="queryToServer('/controller?command=resurrect&entity=account&id=${userEdit.id}',
                                                   '/controller?command=edit&userId=${userEdit.id}',
                                                   'Error during resurrect account. Try again.')"></a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </c:if>
                        <c:choose>
                            <c:when test="${userEdit.id == user.id || user.role == true}"> <%--object == 'owner'--%>
                                <input type="button" value="<fmt:message key="editing" />" id="edit" class="edit"
                                       onClick="myEdit();">
                                <input id="submit" type="submit" value="<fmt:message key="save" />"
                                       class="edit submit-edited-account">
                            </c:when>
                            <c:otherwise>
                                <div class="fake-div"></div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form>
            </main>
            <jsp:include page="footer.jsp" />
            <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
            <script src="${pageContext.request.contextPath}/page/resources/js/editAccount.js"></script>
            <script src="${pageContext.request.contextPath}/page/resources/js/query.js"></script>
        </body>
    </html>
</fmt:bundle>