<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 02.04.2017
  Time: 2:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<jsp:include page="locale.jsp" />
<fmt:bundle basename="pagecontext" prefix = "label." >
    <html>
        <head>
            <title>Participants</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/participants.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/new.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/delete.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/pages.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
            <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        </head>
        <body>
            <jsp:include page="header.jsp" />
            <jsp:include page="avatar.jsp" />
            <main>
                <hr>
                <div class="users-container">
                    <table>
                        <tbody>
                            <c:forEach var="accountIndex" begin="0" end="${fn:length(accounts)}" step="3">
                                <tr>
                                    <c:forEach var="userIndex" begin="${accountIndex}" end="${accountIndex + 2}">
                                        <c:if test="${not empty accounts[userIndex]}">
                                            <td>
                                                <div class="user-info">
                                                    <div class="user-ava">
                                                        <a href="/controller?command=edit&userId=${accounts[userIndex].id}">
                                                            <c:choose>
                                                                <c:when test="${accounts[userIndex].image != 0}">
                                                                    <img src="/controller?command=read_image&image=${accounts[userIndex].image}"
                                                                         alt="ava" width="48px" height="48px">
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <img src="${pageContext.request.contextPath}/page/image/anonymous_avatar.png"
                                                                         alt="ava" width="48px" height="48px">
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </a>
                                                    </div>
                                                    <div class="user-details">
                                                        <a href="/controller?command=edit&userId=${accounts[userIndex].id}">
                                                            ${accounts[userIndex].login}
                                                        </a>
                                                        <div class="user-rating">
                                                            <span title="<fmt:message key="reputation" />">
                                                                ${accounts[userIndex].rating}
                                                            </span>
                                                        </div>
                                                        <div class="new-account">
                                                            <ctg:state-of-entity role="${user.role}"
                                                                                 newEntity="${accounts[userIndex].newEntity}"
                                                                                 active="${accounts[userIndex].active}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </c:if>
                                    </c:forEach>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <jsp:include page="pages.jsp" />
            </main>
            <jsp:include page="footer.jsp" />
            <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
            <c:if test="${countPages > 1}">
                <script>
                    document.getElementById('page').style.float = 'left';
                </script>
            </c:if>
        </body>
    </html>
</fmt:bundle>