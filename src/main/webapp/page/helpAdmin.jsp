<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 29.04.2017
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagecontext" prefix = "label." >      <%--!!!!!!!!!!!!!!!!!--%>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title>Like IT</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/messageForAdmin.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/new.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/delete.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/pages.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="header.jsp" />
    <jsp:include page="avatar.jsp" />
    <main>
        <div class="content">
            <div class="mainbar">
                <c:forEach items="${list1}" var="entity">
                    <div class="record">
                        <div class="contents">
                            <h3>
                                <a href="/controller?command=message_about_help&id=${entity.id}" class="link">
                                    ${entity.title}
                                </a>
                                <c:if test="${entity.newEntity == true}">
                                    <span class="new new-question">new</span>
                                </c:if>
                            </h3>
                            <ctg:date-who object="${entity}" time="${entity.time}" accounts="${accounts}" />
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <jsp:include page="pages.jsp" />
    </main>
    <jsp:include page="footer.jsp" />
    <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
    </body>
    </html>
</fmt:bundle>