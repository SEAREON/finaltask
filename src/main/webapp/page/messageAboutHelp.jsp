<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 01.05.2017
  Time: 21:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagecontext" prefix = "label." >          <%--!!!!!!!!!!!!!!!!!!!!--%>
<html>
    <head>
        <title>Message about help</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/messageForAdmin.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/messageAboutHelp.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/new.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/delete.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
    </head>
    <body>
        <jsp:include page="header.jsp" />
        <jsp:include page="avatar.jsp" />
        <main>
            <hr>
            <div class="message-container">
                <h1>
                    <%--get help title--%>
                    ${object.title}
                    <%--help is new--%>
                    <c:if test="${object.newEntity == true}">
                        <span class="new new-question new-question-with-margin-left">new</span>
                    </c:if>
                </h1>
                <div class="help-text">
                    <%--get help text--%>
                    ${object.text}
                </div>
                <ctg:date-who object="${object}" time="${object.time}" accounts="${accounts}" validate="true"/>
            </div>
        </main>
        <jsp:include page="footer.jsp" />
        <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
        <script src="${pageContext.request.contextPath}/page/resources/js/query.js"></script>
    </body>
</html>
</fmt:bundle>