/**
 * Created by Misha Ro on 03.04.2017.
 */
function getName (str){
    var i;

    if (str.lastIndexOf('\\')){
        i = str.lastIndexOf('\\')+1;
    }
    else{
        i = str.lastIndexOf('/')+1;
    }

    var filename = str.slice(i);
    var uploaded = document.getElementById("file-form-label");
    uploaded.innerHTML = filename;
}

function checkSize() {
    var inputFile = document.getElementById("upload");
    var file = inputFile.files[0];

    if(file.size > 2000000) {
        alert('максимальный размер изображения может быть не более 2 мб');
    }
}

function myEdit() {
    document.getElementById('file-form').style.display = 'block';
    var description = document.getElementById('div-discription');
//    document.getElementById('area-description').innerHTML = description.children[0].innerHTML;
    document.getElementById('area-description').style.display = 'block';
    description.style.display = 'none';
    document.getElementById('submit').style.display = 'block';
    document.getElementById('edit').style.display = 'none';

    appendToInput('user-card-name-id', 'user-card-name-edit-id');
    appendToInput('email-user', 'email-user-edit');

    document.getElementsByTagName('main')[0].style.paddingBottom = '77px';

    var deleteButton = document.getElementById('delete-user-ava');
    if (deleteButton != null) {
        deleteButton.style.display = 'block';
    }

    document.getElementById('new').style.display = 'none';
}

function appendToInput(fromId, toId) {
//    var from = document.getElementById(fromId);
//    var to = document.getElementById(toId);
//    document.getElementById(toId).value = document.getElementById(fromId).innerHTML;
    document.getElementById(fromId).style.display = 'none';
    document.getElementById(toId).style.display = 'block';
}

function deleteAva(idImage) {
    var button = document.getElementById('delete-user-ava');
    var xhr = new XMLHttpRequest();

    xhr.open('post', '/controller?command=delete_image&id=' + idImage, true);

    xhr.send();

    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            alert('sorry, we have some problems on server');
            button.disabled = false;
            button.value = 'Удалить фото';
            button.style.backgroundColor = '#07C';
        } else {
            document.getElementById('user-ava').src =
                'http://www.blogopen.rs/wp-content/uploads/2015/03/anonymous_avatar.png';
            button.style.display = 'none';
        }
    }

    button.value = 'удаление'; // (2)
    button.disabled = true;
    button.style.backgroundColor = 'lightgray';
}
