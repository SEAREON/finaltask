/**
 * Created by Misha Ro on 02.05.2017.
 */
function queryToServer(uri, redirectURI, errorMessage) {
    var xhr = new XMLHttpRequest();

    xhr.open('post', uri, true);

    xhr.send();

    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            alert(errorMessage);
        } else {
            var f = document.createElement("form");

            var path_param = redirectURI.split(/\?/);
            var param = path_param[1].split(/&/);

            for(var paramIndex = 0; paramIndex < param.length; paramIndex++) {
                var input = document.createElement("input");
                input.setAttribute('type', 'hidden');

                var name_value = param[paramIndex].split(/=/);
                input.setAttribute('name', name_value[0]);
                input.setAttribute('value', name_value[1]);

                f.appendChild(input);
            }

            f.setAttribute('method', "get");
            f.setAttribute('action', path_param[0]);

            document.body.appendChild(f);

            f.submit();
        }
    }
}
