/**
 * Created by Misha Ro on 28.04.2017.
 */
function deleteAnswer(id) {
    document.getElementsByTagName('table-' + id).style.backgroundColor = "gray";
    document.getElementsByTagName('status-' + id).innerHTML = "delete";
    document.getElementsByTagName('status-' + id).style.color = "white";
    document.getElementsByTagName('up-' + id).style.display = "none";
    document.getElementsByTagName('mark-' + id).style.color = "black";
    document.getElementsByTagName('down-' + id).style.display = "none";
}
