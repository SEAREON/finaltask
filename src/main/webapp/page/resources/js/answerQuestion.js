/**
 * Created by Misha Ro on 09.04.2017.
 */
function prepareAnswer() {
    var area = document.getElementsByTagName('textarea')[0].value;
    var answer = '';

    area = area.replace(/^\s+/, "");        // .trim()

    for(var index = 0; index < area.length; index++) {
        if(area[index] === '/n') {
            answer = '<br>';
        } else {
            answer += area[index];
        }
    }

    document.getElementsByTagName('textarea')[0].value = answer;
}

function checkAnswerForm(form, id) {
    if(document.getElementById('answer-form-body-' + id).value == '') {

        alert('Fill in the reply field.');
        return false;
    }

    return true;
}