/**
 * Created by Misha Ro on 28.03.2017.
 */
function checkAuthorization(user) {
    if(user.length > 0) {
        document.getElementById('error-info').style.display = 'none';
        document.getElementById('ask-question-submit').disabled = false;
        document.getElementById('ask-question-submit').style.backgroundColor = '#07C';
        document.getElementById('ask-question-submit').style.cursor = 'pointer';
    }
}

function checkForm(form) {
    if(document.getElementById('question-form-title').value == '' ||
        document.getElementById('question-form-body').value == '' ||
        document.getElementById('question-form-topics').value == '') {

        alert('Fill in all the fields.');
        return false;
    }

    return true;
}