/**
 * Created by Misha Ro on 23.03.2017.
 */
function getName (str){
    var i;

    if (str.lastIndexOf('\\')){
        i = str.lastIndexOf('\\')+1;
    }
    else{
        i = str.lastIndexOf('/')+1;
    }

    var filename = str.slice(i);
    var uploaded = document.getElementById("file-form-label");
    uploaded.innerHTML = filename;
}

function checkSize() {
    var inputFile = document.getElementById("upload");
    var file = inputFile.files[0];

    if(file.size > 2000000) {
        document.getElementById('message-error-image').style.display = 'block';
    } else {
        document.getElementById('message-error-image').style.display = 'none';
    }
}

function preSubmit() {
    if(document.getElementById('message-error-image').style.display == 'block') {
        var inputFile = document.getElementById("upload");
        inputFile.parentNode.removeChild(inputFile);
    }
}

function infoFocus(id) {
    document.getElementById(id).style.display = 'block';
}

function infoBlur(id) {
    document.getElementById(id).style.display = 'none';
}