/**
 * Created by Misha Ro on 14.04.2017.
 */
function edit(type, id) {
    if(type === "question") {
        document.getElementById('question-form-title').value =
            document.getElementById('question-form-title').value.trim();

        var topics = document.getElementById('question-topics').childNodes;
        str = "";

        for(var topicIndex = 0; topicIndex < topics.length; topicIndex++) {
            if(topics[topicIndex].localName === 'a') {
                if(str.length == 0) {
                    str += topics[topicIndex].innerHTML.trim();
                } else {
                    str += ' ' + topics[topicIndex].innerHTML.trim();
                }
            }
        }

        document.getElementById('question-form-topics').value = str;
        document.getElementById('question-form-old-topics').value = str;

        document.getElementById('question-form').style.display = 'block';
        document.getElementById('div-question').style.display ='none';
        document.getElementById('question-edit-image').style.display = 'none';
    } else {
        /*var answerText = document.getElementById('answer-' + id).innerHTML;
        document.getElementById('answer-form-body-' + id).innerHTML = answerText.replace(/^\s+/, "");*/
        document.getElementById('answer-form-' + id).style.display = 'block';
        document.getElementById('rating-' + id).style.display ='none';
        document.getElementById('answer-' + id).style.display ='none';
        document.getElementById('answer-edit-image-' + id).style.display ='none';
    }
}