/**
 * Created by Misha Ro on 22.03.2017.
 */
function show(state, window){
    if(window != null) {
        document.getElementById(window).style.display = state;
        document.getElementById('wrap').style.opacity = '0.7';
    } else {
        document.getElementById('window-login').style.display = state;
        document.getElementById('window-language').style.display = state;
        document.getElementById('window-help').style.display = state;
        document.getElementById('wrap').style.opacity = '0';
    }

    document.getElementById('wrap').style.display = state;
}