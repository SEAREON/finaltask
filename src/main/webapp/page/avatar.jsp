<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 01.04.2017
  Time: 10:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Avatar</title>
</head>
<body>
    <div class="in-main-header">
        <div class="container-logo">
            <div class="logo">
                <img src="${pageContext.request.contextPath}/page/image/it.png"
                     width="90" height="80" alt="logo">
            </div>
            <c:choose>
                <c:when test="${not empty user}">
                    <a href="/controller?command=edit&userId=${user.id}">
                        <div class="user">
                            <span class="user-name">
                                ${user.login}
                            </span>
                            <c:choose>
                                <c:when test="${user.image != 0}">
                                    <img src="/controller?command=read_image&image=${user.image}"
                                         width="80" height="80" class="ava" alt="ava">
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/page/image/anonymous_avatar.png"
                                         width="80" height="80" class="ava" alt="ava">
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </a>
                </c:when>
                <c:otherwise>
                    <div class="user">
                        <img src="${pageContext.request.contextPath}/page/image/anonymous_avatar.png"
                             width="80" height="80" class="ava" alt="ava">
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</body>
</html>
