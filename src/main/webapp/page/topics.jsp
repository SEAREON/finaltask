<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 01.04.2017
  Time: 10:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="pagecontext" prefix = "label." >
    <html>
        <head>
            <title>Topics</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/header.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/topics.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/pages.css">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/page/resources/css/footer.css">
            <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        </head>
        <body>
            <jsp:include page="header.jsp" />
            <jsp:include page="avatar.jsp" />
            <main>
                <hr>
                <div class="topics-container">
                    <c:forEach items="${topics}" var="topic">
                        <div class="tag">
                            <a href="/controller?command=questions_by_topic&id=${topic.id}"
                               class="post-tag">
                                ${topic.text}
                            </a>
                            <span class="topic-counter">
                                x ${topic.countQuestion}
                            </span>
                        </div>
                    </c:forEach>
                </div>
                <jsp:include page="pages.jsp" />
            </main>
            <jsp:include page="footer.jsp" />
            <script src="${pageContext.request.contextPath}/page/resources/js/header.js"></script>
        </body>
    </html>
</fmt:bundle>