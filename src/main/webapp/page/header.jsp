<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 21.03.2017
  Time: 22:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:bundle basename="pagecontext" prefix = "label." >              <%--!!!!!!!!!!!!!!!--%>
<html>
    <head>
    </head>
    <body>
        <div onclick="show('none')" id="wrap"></div>
        <header>
            <div class="topbar-wrapper">
                <div class="links">
                    <div class="links-container">
                        <span class="topbar-menu-links">
                            <c:choose>
                                <c:when test="${not empty user}">
                                    <a href="/controller?command=logout">
                                        <fmt:message key="exit" />
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <a href="#" id="login-logout" onclick="show('block', 'window-login')">
                                        <fmt:message key="entry" />
                                            <span class="triangle"/>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                            <a href="#" id="language-dialog" onclick="show('block', 'window-language')">
                                <fmt:message key="language" />
                                <span class="triangle"/>
                            </a>
                            <c:if test="${currentPage != '/page/registration.jsp'}">
                                            <a href="${pageContext.request.contextPath}/page/registration.jsp" >
                                                <fmt:message key="registration"/>
                                            </a>
                            </c:if>
                            <div id="window-login" class="topbar-dialog topbar-dialog-login">
                                <form action="/controller" method="get" class="modal-content-login">
                                    <input type="hidden" name="command" value="login">
                                    <label for="login" class="topbar-dialog-elem">
                                        <fmt:message key="login" />:
                                    </label>
                                    <input type="text" name="login" id="login" class="input-fields" />
                                    <label for="password" class="topbar-dialog-elem">
                                        <fmt:message key="password" />:
                                    </label>
                                    <input type="password" name="password" id="password" class="input-fields" />
                                    <div class="topbar-dialog-elem">
                                        <input type="submit" value="<fmt:message key="entry" />"
                                               class="topbar-dialog-submit">
                                        <div class="error-message">
                                            <span id = "error">
                                                ${errorLogPassMessage}
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <c:if test="${show == 'login'}" >
                                <script>
                                    document.getElementById('window-login').style.display = 'block';
                                    document.getElementById('wrap').style.display = 'block';
                                </script>
                            </c:if>
                                <a href="/controller?command=index&currentPage=1" >
                                    <fmt:message key="questions" />
                                </a>
                            <a href="/controller?command=topics&currentPage=1" >
                                <fmt:message key="topics" />
                            </a>
                            <a href="/controller?command=participants&currentPage=1" >
                                <fmt:message key="participants" />
                            </a>
                            <a href="/controller?command=question_without_answers&currentPage=1" >
                                <fmt:message key="unanswered" />
                            </a>
                            <a href="${pageContext.request.contextPath}/page/askQuestion.jsp">
                                <fmt:message key="ask_question" />
                            </a>
                        </span>
                    </div>
                    <div id="window-language" class="topbar-dialog topbar-dialog-language" >
                        <div class="modal-content">
                            <ul>
                                <li class="top-li">
                                    <a href="/controller?command=language&language=en" class="language">
                                        English
                                    </a>
                                </li>
                                <li>
                                    <a href="/controller?command=language&language=ru" class="language">
                                        Русский
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="topbar-search">
                    <form id="search" action="/controller" method="get" autocomplete="off">
                        <input type="hidden" name="command" value="search_question_by_part_of_title">
                        <input type="hidden" name="currentPage" value="1">
                        <input name="part-of-title" type="text"
                               placeholder="<fmt:message key="search" />"
                               tabindex="1" autocomplete="off" maxlength="240">
                    </form>
                </div>
            </div>
        </header>
        <c:if test="${not empty errorMessage}">
            <script>
                alert("${errorMessage}");
            </script>
        </c:if>
    </body>
</html>
</fmt:bundle>