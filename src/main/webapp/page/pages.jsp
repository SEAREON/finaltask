<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 06.04.2017
  Time: 21:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagecontext" prefix = "label." >
<html>
    <head>
    </head>
    <body>
    <%--if count of pages > 1--%>
    <c:if test="${countPages > 1}">
        <div class="page" id="page">
            <c:if test="${currentPageNumber > 1}">
                <a href="${pageCommand}${currentPageNumber - 1}"
                   title="<fmt:message key="go.to.page" /> ${currentPageNumber - 1}">
                    <span class="page-number"><fmt:message key="back" /></span>
                </a>
            </c:if>
            <c:choose>
                <%--if count of pages > 7--%>
                <c:when test="${countPages > 7}">
                    <c:choose>
                        <%--if current page < 5--%>
                        <c:when test="${currentPageNumber < 5}">
                            <ctg:page-number begin="1" end="5" currentPageNumber="${currentPageNumber}"
                                             command="${pageCommand}" />

                            <span class="page-number dots">...</span>
                            <a href="${pageCommand}${countPages}"
                               title="<fmt:message key="go.to.page" /> ${countPages}">
                                <span class="page-number">${countPages}</span>
                            </a>
                        </c:when>
                        <%--if current page > count op pages - 4--%>
                        <c:when test="${currentPageNumber > countPages - 4}">
                            <a href="${pageCommand}1" title="<fmt:message key="go.to.page" /> 1">
                                <span class="page-number">1</span>
                            </a>
                            <span class="page-number dots">...</span>
                            <%--begin from count of pages - 4 to count of pages--%>
                            <ctg:page-number begin="${countPages - 4}" end="${countPages}"
                                             currentPageNumber="${currentPageNumber}"
                                             command="${pageCommand}" />
                        </c:when>
                        <%--if current page < 4 and current page < count pages - 4--%>
                        <c:when test="${currentPageNumber > 4 && currentPageNumber < countPages - 4}">
                            <a href="${pageCommand}1" title="<fmt:message key="go.to.page" /> 1">
                                <span class="page-number">1</span>
                            </a>
                            <span class="page-number dots">...</span>
                            <a href="${pageCommand}${currentPageNumber - 1}"
                               title="<fmt:message key="go.to.page" /> ${currentPageNumber - 1}">
                                <span class="page-number">${currentPageNumber - 1}</span>
                            </a>
                            <span class="page-number current">${currentPageNumber}</span>
                            <a href="${pageCommand}${currentPageNumber + 1}"
                               title="<fmt:message key="go.to.page" /> ${currentPageNumber + 1}">
                                <span class="page-number">${currentPageNumber + 1}</span>
                            </a>
                            <span class="page-number dots">...</span>
                            <a href="${pageCommand}${countPages}"
                               title="<fmt:message key="go.to.page" /> ${countPages}">
                                <span class="page-number">${countPages}</span>
                            </a>
                        </c:when>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <%--end = count of pages--%>
                    <ctg:page-number begin="1" end="${countPages}"
                                     currentPageNumber="${currentPageNumber}"
                                     command="${pageCommand}" />
                </c:otherwise>
            </c:choose>
            <c:if test="${currentPageNumber != countPages}">
                <a href="${pageCommand}${currentPageNumber + 1}"
                   title="<fmt:message key="go.to.page" /> ${currentPageNumber + 1}">
                    <span class="page-number"><fmt:message key="further" /></span>
                </a>
            </c:if>
        </div>
    </c:if>
    </body>
</html>
</fmt:bundle>