<%--
  Created by IntelliJ IDEA.
  User: Misha Ro
  Date: 21.03.2017
  Time: 22:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:bundle basename="pagecontext" prefix = "label." >              <%--!!!!!!!!!!!!--%>
<html>
    <head>
    </head>
    <body>
        <footer>
            <div class="dialog footer-dialog-help" id="window-help">
                <div class="error-create-help-query" id="without-login">
                    <span class="text-info">
                        <fmt:message key="log.in.or.register" />
                    </span>
                </div>
                <form action="/controller" method="post">
                    <input type="hidden" name="command" value="ask_for_help">
                    <input type="hidden" name="owner" value="${user.id}">
                    <label for="header-form-help">
                        <fmt:message key="header" />
                    </label>
                    <input type="text" id="header-form-help" name="header"
                           placeholder="<fmt:message key="kind.of.help" />">
                    <textarea maxlength="1000" class="body-text" name="help-body"></textarea>
                    <input class="form-helper-submit" type="submit" value="<fmt:message key="submit"/>"
                           id="form-help-submit">
                </form>
            </div>
            <div class="footer-wrapper">
                <div class="footer-menu">
                    <c:choose>
                        <c:when test="${user.role == true}">
                            <a href="/controller?command=search_help&currentPage=1">
                                <fmt:message key="help.records"/>
                            </a>
                            <a href="/controller?command=search_new_help&currentPage=1">
                                <fmt:message key="new.help.notes" />
                            </a>
                            <a href="/controller?command=search_new_questions&currentPage=1">
                                <fmt:message key="new.questions" />
                            </a>
                            <a href="/controller?command=search_questions_with_new_answers&currentPage=1">
                                <fmt:message key="new.replies" />
                            </a>
                            <a href="/controller?command=new_participants&currentPage=1">
                                <fmt:message key="new.members" />
                            </a>
                            <a href="/controller?command=search_delete_questions&currentPage=1">
                                <fmt:message key="deleted.questions" />
                            </a>
                            <a href="/controller?command=search_questions_with_delete_answers&currentPage=1">
                                <fmt:message key="deleted.answers" />
                            </a>
                            <a href="/controller?command=delete_participants&currentPage=1">
                                <fmt:message key="deleted.members" />
                            </a>
                        </c:when>
                        <c:otherwise>
                            <a onclick="show('block', 'window-help')" href="#">
                                <fmt:message key="help" />
                            </a>
                            <a href="#">
                                <fmt:message key="about_us" />
                            </a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </footer>
        <c:if test="${user == null}">
            <script>
                document.getElementById('without-login').style.display = 'block';
                document.getElementById('form-help-submit').style.background = "grey";
                document.getElementById('form-help-submit').disabled = 'true';
            </script>
        </c:if>
    </body>
</html>
</fmt:bundle>