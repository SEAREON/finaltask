package com.seareon.servlet.tag;

import com.seareon.resource.MessageManager;
import com.seareon.resource.Util;
import com.seareon.servlet.Controller;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The class implements a custom tag for displaying page tag.
 * @version 1.0
 * @author Misha Ro
 */
public class PageNumber extends TagSupport {
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * The starting page count.
     */
    private int begin;
    /**
     * End of page count.
     */
    private int end;
    /**
     * Current page number.
     */
    private int currentPageNumber;
    /**
     * A command to go to a certain page.
     */
    private String command;

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public void setCurrentPageNumber(int currentPageNumber) {
        this.currentPageNumber = currentPageNumber;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * Override {@link TagSupport#doStartTag()}
     * @return
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        JspWriter writer = pageContext.getOut();
        String answer;
        String title =  MessageManager.getProperty("pagecontext", "label.go.to.page");

        for(int pageIndex = begin; pageIndex <= end; pageIndex++) {
            try {
                if(pageIndex == currentPageNumber) {
                    answer = Util.getAnswer("<span class=\"page-number current\">", String.valueOf(pageIndex),
                            "</span>");
                } else {
                    answer = Util.getAnswer("<a href=\"", command, String.valueOf(pageIndex), "\" title=\"", title, " ",
                            String.valueOf(pageIndex), "\"><span class=\"page-number\">", String.valueOf(pageIndex),
                            "</span></a>");
                }

                writer.write(answer);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

        return SKIP_BODY;
    }
}
