package com.seareon.servlet.tag;

import com.seareon.servlet.Controller;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The class implements a custom tag for displaying object state tag.
 * @version 1.0
 * @author Misha Ro
 */
public class StateOfEntity extends TagSupport {
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * Defines the user role.
     */
    private boolean role;

    /**
     * Determines the state of novelty of an object.
     */
    private boolean newEntity;

    /**
     * Determines the state of the object's activity.
     */
    private boolean active;

    /**
     * The number of new answers.
     */
    private int newAnswers;

    /**
     * The number of delete answers.
     */
    private int deleteAnswers;

    public void setRole(boolean role) {
        this.role = role;
    }

    public void setNewEntity(boolean newEntity) {
        this.newEntity = newEntity;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setNewAnswers(int newAnswers) {
        this.newAnswers = newAnswers;
    }

    public void setDeleteAnswers(int deleteAnswers) {
        this.deleteAnswers = deleteAnswers;
    }

    /**
     * Override {@link TagSupport#doStartTag()}
     * @return
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        try {
            if(role) {
                JspWriter writer = pageContext.getOut();

                if(!active) {
                    writer.write("<span class=\"delete new-question\">delete</span>");
                } else {
                    if(newEntity) {
                        writer.write("<span class=\"new new-question\">new</span>");
                    }
                    if(newEntity && newAnswers > 0) {
                        writer.write("<span class=\"new new-question\"> / </span>");
                    }
                    if(newAnswers > 0) {
                        writer.write("<span class=\"new new-question\">new answer</span>");
                    }
                    if((newAnswers > 0 && deleteAnswers > 0) || (newEntity && deleteAnswers > 0)) {
                        writer.write("<span class=\"new new-question\"> / </span>");
                    }
                    if(deleteAnswers > 0) {
                        writer.write("<span class=\"delete new-question\">delete answer</span>");
                    }
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new JspException(e.getMessage());
        }

        return SKIP_BODY;
    }


}
