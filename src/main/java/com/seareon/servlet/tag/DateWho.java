package com.seareon.servlet.tag;

import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.AccountLink;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.account.link.impl.Help;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.resource.MessageManager;
import com.seareon.resource.Util;
import com.seareon.servlet.Controller;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * The class implements a custom tag for displaying the date, user and user rating.
 * @version 1.0
 * @author Misha Ro
 */
public class DateWho extends TagSupport {
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * User reference.
     */
    private AccountLink object;

    /**
     * Availability of validation.
     */
    private boolean validate;

    /**
     * Date object.
     */
    private Date time;

    /**
     * List of Accounts.
     */
    private List<Account> accounts;

    /**
     * Availability of editing.
     */
    private String edit;

    public void setObject(AccountLink object) {
        this.object = object;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setAccounts(List accounts) {
        this.accounts = (List<Account>) accounts;
    }

    public void setEdit(String edit) { this.edit = edit; }

    /**
     * Override {@link TagSupport#doStartTag()}
     * @return
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        JspWriter writer = pageContext.getOut();
        Account user = (Account) pageContext.getSession().getAttribute(Constant.USER);
        String answer = "<div class=\"date-who\">";

        try {
            if(user != null && user.isRole() && object.isNewEntity() && (object instanceof Help || object.isActive())
                    && validate) {
                answer += getValidate(object);
            }

            for(Account account : accounts) {
                if(account.getId() == object.getAccountId()) {
                    answer = Util.getAnswer(answer, "<span class=\"date\">",
                        dateToString(time), "</span>");

                    if(account.isActive()) {
                        answer = Util.getAnswer(answer, "<a href=\"/controller?command=edit&userId=",
                            String.valueOf(account.getId()), "\" class=\"who link\">", account.getLogin(),
                            "</a><span class=\"who-reputation\"> ", String.valueOf(account.getRating()), "</span>");
                    }

                    if(edit != null) {
                        answer += getEdit(object);
                    }

                    break;
                }
            }

            answer = Util.getAnswer(answer, "</div>");
            writer.write(answer);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return SKIP_BODY;
    }

    /**
     * The method translates the date into a string.
     * @param time - Date object.
     * @return - String object.
     */
    private String dateToString(Date time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(time);
    }

    /**
     * The method returns a string with the validation tag.
     * @param object - AccountLink object.
     * @return - String object.
     */
    private String getValidate(AccountLink object) {
        String answer = "<a onclick=\"queryToServer('/controller?command=";

        if(object instanceof Question) {
            answer = Util.getAnswer(answer, "question_verified&id=",
                    String.valueOf(object.getId()), "', '/controller?command=answer_question&id=",
                    String.valueOf(object.getId()), "', " +
                            "'Error during verified question. Try again.')\" id=\"verified\">verified</a>");
        } else if(object instanceof Answer) {
            answer = Util.getAnswer(answer, "answer_verified&id=",
                    String.valueOf(object.getId()), "', '/controller?command=answer_question&id=",
                    String.valueOf(((Answer) object).getQuestionId()), "', " +
                            "'Error during verified qanswer. Try again.')\" id=\"verified\">verified</a>");
        } else {
            answer = Util.getAnswer(answer,"help_verified&id=", String.valueOf(object.getId()),
                    "', '/controller?command=message_about_help&id=", String.valueOf(object.getId()),
                    "', 'Error during read help message. Try again.')\" id=\"verified\">readed</a>");
        }

        return answer;
    }

    /**
     * The method returns a string with the edit tag.
     * @param object - AccountLink object.
     * @return - String object.
     */
    private String getEdit(AccountLink object) {
        Account user = (Account) pageContext.getSession().getAttribute("user");
        String answer = "";
        if(user != null && (user.getId() == object.getAccountId()/*.getId()*/ || user.isRole())) {
            answer = "<a class=\"edit-image edit-pancil\" id=\"";
            String pencilTitle = MessageManager.getProperty("pagecontext",
                    "label.pencil");

            if (object instanceof Question) {
                answer = Util.getAnswer(answer, "question-edit-image\" title=\"",  pencilTitle,  "\" ",
                        "onclick=\"edit('question')\"></a>");
                answer +=  getAct(object, "question");
            } else {
                answer = Util.getAnswer(answer, "answer-edit-image-", String.valueOf(object.hashCode()),
                        "\"  title=\"", pencilTitle, "\" onclick=\"edit('answer', ", String.valueOf(object.hashCode()),
                        ")\"></a>");
                answer +=  getAct(object, "answer");
            }
        }

        return answer;
    }

    /**
     * The method returns a string with tag indicating the owner.
     * @param object - AccountLink object.
     * @param type - The type of the entity for which the tag is created.
     * @return - String object.
     */
    private  String getAct(AccountLink object, String type) {
        String answer = Util.getAnswer("<a class=\"edit-image ");
        String actTitle;

        if(object.isActive()) {
            actTitle = MessageManager.getProperty("pagecontext", "label.delete");

            answer = Util.getAnswer(answer, "edit-delete\" title=\"", actTitle,
                    "\" onclick=\"queryToServer('/controller?command=delete&entity=", type);
        } else {
            actTitle = MessageManager.getProperty("pagecontext", "label.resurrect");

            answer = Util.getAnswer(answer, "resurrect\" title=\"", actTitle,
                    "\" onclick=\"queryToServer('/controller?command=resurrect&entity=", type);
        }

        answer = Util.getAnswer(answer, "&id=",
                String.valueOf(object.getId()), "', '/controller?command=answer_question&id=");

        if(object instanceof Question) {
            answer = Util.getAnswer(answer, String.valueOf(object.getId()));
        } else {
            answer = Util.getAnswer(answer, String.valueOf(((Answer)object).getQuestionId()));
        }

        answer = Util.getAnswer(answer, "', 'Error during delete ", type, ". Try again.')\"></a>");

        return answer;
    }
}
