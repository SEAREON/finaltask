package com.seareon.servlet.command;

import com.seareon.resource.ConfigurationManager;
import com.seareon.resource.Constant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * An abstract method that defines the interface for query processing classes.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class ActionCommand {
    /**
     * The method executes the processing of requests from the client.
     * @param var1 - HttpServletRequest object.
     * @param var2 - HttpServletResponse object.
     * @throws IOException
     */
    public abstract void execute(HttpServletRequest var1, HttpServletResponse var2) throws IOException;

    /**
     * The method sets the page for selecting the jsp that is needed.
     * @param request - HttpServletRequest object.
     * @param propertyPath - HttpServletResponse object.
     * @throws IOException
     */
    protected void setCurrentPage(HttpServletRequest request, String propertyPath) throws IOException {
        String page = (String)ConfigurationManager.getProperties(Constant.APP_CONFIG,
                new String[]{propertyPath}).get(propertyPath);
        request.getSession().setAttribute(Constant.CURRENT_PAGE, page);
    }

    /**
     * The method checks the correctness of the parameters in the client request.
     * @param request - HttpServletRequest object.
     * @return - If all query parameters are correct then true otherwise false.
     */
    protected boolean verify(HttpServletRequest request) {
        Enumeration params = request.getParameterNames();

        String value;
        do {
            if(!params.hasMoreElements()) {
                return true;
            }

            String paramName = (String)params.nextElement();
            value = request.getParameter(paramName);
        } while(!value.isEmpty());

        return false;
    }
}
