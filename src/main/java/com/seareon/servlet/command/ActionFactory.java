package com.seareon.servlet.command;

import com.seareon.resource.Constant;
import com.seareon.servlet.Controller;
import com.seareon.servlet.command.client.CommandEnum;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * The factory class returns a command object by name.
 * @version 1.0
 * @author Misha Ro
 */
public class ActionFactory {
    private static final Logger logger = Logger.getLogger(Controller.class);
    private static final String COM_STR_LOGGER = " --- command: ";

    /**
     * The method returns an ActionCommand by name.
     * @param request - HttpServletRequest object.
     * @return - Command object.
     */
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = null;
        String action = request.getParameter(Constant.COMMAND);
        logger.info(request.getSession().getId() + COM_STR_LOGGER + action);

        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            logger.error(request.getSession().getId() + Constant.STR_LOGGER + e.getMessage() + Constant.STR_LOGGER_TAB);

            current = CommandEnum.WRONG.getCurrentCommand();
        }

        return current;
    }
}
