package com.seareon.servlet.command.client;

import com.seareon.servlet.command.ActionCommand;
import com.seareon.servlet.command.impl.*;
import com.seareon.servlet.command.impl.active.impl.DeleteCommand;
import com.seareon.servlet.command.impl.active.impl.ResurrectCommand;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.AnswerQuestionCommand;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.SearchDeleteQuestionCommand;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.SearchNewQuestionCommand;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.impl.topic.menu.*;
import com.seareon.servlet.command.impl.enumerable.participant.DeleteParticipantsCommand;
import com.seareon.servlet.command.impl.enumerable.participant.NewParticipantsCommand;
import com.seareon.servlet.command.impl.enumerable.participant.ParticipantsCommand;
import com.seareon.servlet.command.impl.image.impl.picture.RegistrationCommand;
import com.seareon.servlet.command.impl.image.impl.picture.SaveChangesAccountCommand;
import com.seareon.servlet.command.impl.enumerable.owner.help.SearchHelpCommand;
import com.seareon.servlet.command.impl.enumerable.owner.help.SearchNewHelpCommand;
import com.seareon.servlet.command.impl.verifying.impl.verifying.AccountVerifiedCommand;
import com.seareon.servlet.command.impl.verifying.impl.verifying.AnswerVerifiedCommand;
import com.seareon.servlet.command.impl.verifying.impl.verifying.HelpVerfiedCommand;
import com.seareon.servlet.command.impl.verifying.impl.verifying.QuestionVerifiedCommand;
import com.seareon.servlet.command.impl.topics.impl.work.NewQuestion;
import com.seareon.servlet.command.impl.topics.impl.work.UpdateQuestionCommand;

/**
 * Enumeration contains commands for processing requests from the servlet.
 * @version 1.0
 * @author Misha Ro
 */
public enum CommandEnum {
    /**
     * Encapsulating {@link LoginCommand}
     */
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    /**
     * Encapsulating {@link LogoutCommand}
     */
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    /**
     * Encapsulating {@link IndexCommand}
     */
    INDEX {
        {
            this.command = new IndexCommand();
        }
    },
    /**
     * Encapsulating {@link WrongAction}
     */
    WRONG {
        {
            this.command = new WrongAction();
        }
    },
    /**
     * Encapsulating {@link LanguageCommand}
     */
    LANGUAGE {
        {
            this.command = new LanguageCommand();
        }
    },
    /**
     * Encapsulating {@link RegistrationCommand}
     */
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    /**
     * Encapsulating {@link ReadImage}
     */
    READ_IMAGE {
        {
            this.command = new ReadImage();
        }
    },
    /**
     * Encapsulating {@link NewQuestion}
     */
    NEW_QUESTION {
        {
            this.command = new NewQuestion();
        }
    },
    /**
     * Encapsulating {@link SearchNewQuestionCommand}
     */
    SEARCH_NEW_QUESTIONS {
        {
            this.command = new SearchNewQuestionCommand();
        }
    },
    /**
     * Encapsulating {@link SearchDeleteQuestionCommand}
     */
    SEARCH_DELETE_QUESTIONS {
        {
            this.command = new SearchDeleteQuestionCommand();
        }
    },
    /**
     * Encapsulating {@link SearchQuestionWithNewAnswersCommand}
     */
    SEARCH_QUESTIONS_WITH_NEW_ANSWERS {
        {
            this.command = new SearchQuestionWithNewAnswersCommand();
        }
    },
    /**
     * Encapsulating {@link SearchQuestionWithDeleteAnswersCommand}
     */
    SEARCH_QUESTIONS_WITH_DELETE_ANSWERS {
        {
            this.command = new SearchQuestionWithDeleteAnswersCommand();
        }
    },
    /**
     * Encapsulating {@link SearchHelpCommand}
     */
    SEARCH_HELP {
        {
            this.command = new SearchHelpCommand();
        }
    },
    /**
     * Encapsulating {@link SearchNewHelpCommand}
     */
    SEARCH_NEW_HELP {
        {
            this.command = new SearchNewHelpCommand();
        }
    },
    /**
     * Encapsulating {@link TopicsCommand}
     */
    TOPICS {
        {
            this.command = new TopicsCommand();
        }
    },
    /**
     * Encapsulating {@link ParticipantsCommand}
     */
    PARTICIPANTS {
        {
            this.command = new ParticipantsCommand();
        }
    },
    /**
     * Encapsulating {@link NewParticipantsCommand}
     */
    NEW_PARTICIPANTS {
        {
            this.command = new NewParticipantsCommand();
        }
    },
    /**
     * Encapsulating {@link DeleteParticipantsCommand}
     */
    DELETE_PARTICIPANTS {
        {
            this.command = new DeleteParticipantsCommand();
        }
    },
    /**
     * Encapsulating {@link EditAccountCommand}
     */
    EDIT {
        {
            this.command = new EditAccountCommand();
        }
    },
    /**
     * Encapsulating {@link DeleteImageCommand}
     */
    DELETE_IMAGE {
        {
            this.command = new DeleteImageCommand();
        }
    },
    /**
     * Encapsulating {@link SaveChangesAccountCommand}
     */
    SAVE_CHANGES_ACCOUNT {
        {
            this.command = new SaveChangesAccountCommand();
        }
    },
    /**
     * Encapsulating {@link AnswerQuestionCommand}
     */
    ANSWER_QUESTION {
        {
            this.command = new AnswerQuestionCommand();
        }
    },
    /**
     * Encapsulating {@link RatingCommand}
     */
    RATING {
        {
            this.command = new RatingCommand();
        }
    },
    /**
     * Encapsulating {@link AnswerCommand}
     */
    ANSWER {
        {
            this.command = new AnswerCommand();
        }
    },
    /**
     * Encapsulating {@link UpdateAnswerCommand}
     */
    UPDATE_ANSWER {
        {
            this.command = new UpdateAnswerCommand();
        }
    },
    /**
     * Encapsulating {@link UpdateQuestionCommand}
     */
    UPDATE_QUESTION {
        {
            this.command = new UpdateQuestionCommand();
        }
    },
    /**
     * Encapsulating {@link QuestionByTopicCommand}
     */
    QUESTIONS_BY_TOPIC {
        {
            this.command = new QuestionByTopicCommand();
        }
    },
    /**
     * Encapsulating {@link QuestionVerifiedCommand}
     */
    QUESTION_VERIFIED {
        {
            this.command = new QuestionVerifiedCommand();
        }
    },
    /**
     * Encapsulating {@link AnswerVerifiedCommand}
     */
    ANSWER_VERIFIED {
        {
            this.command = new AnswerVerifiedCommand();
        }
    },
    /**
     * Encapsulating {@link AccountVerifiedCommand}
     */
    ACCOUNT_VERIFIED {
        {
            this.command = new AccountVerifiedCommand();
        }
    },
    /**
     * Encapsulating {@link HelpVerfiedCommand}
     */
    HELP_VERIFIED {
        {
            this.command = new HelpVerfiedCommand();
        }
    },
    /**
     * Encapsulating {@link CreatingRequestForHelpCommand}
     */
    ASK_FOR_HELP {
        {
            this.command = new CreatingRequestForHelpCommand();
        }
    },
    /**
     * Encapsulating {@link CreatingRequestForHelpCommand}
     */
    DELETE {
        {
            this.command = new DeleteCommand();
        }
    },
    /**
     * Encapsulating {@link ResurrectCommand}
     */
    RESURRECT {
        {
            this.command = new ResurrectCommand();
        }
    },
    /**
     * Encapsulating {@link QuestionWithoutAnswerCommand}
     */
    QUESTION_WITHOUT_ANSWERS {
        {
            this.command = new QuestionWithoutAnswerCommand();
        }
    },
    /**
     * Encapsulating {@link HelpCommand}
     */
    MESSAGE_ABOUT_HELP {
        {
            this.command = new HelpCommand();
        }
    },
    /**
     * Encapsulating {@link ChangeAccountRoleCommand}
     */
    CHANGE_USER_ROLE {
        {
            this.command = new ChangeAccountRoleCommand();
        }
    },
    /**
     * Encapsulating {@link QuestionByPartOfTitleCommand}
     */
    SEARCH_QUESTION_BY_PART_OF_TITLE {
        {
            this.command = new QuestionByPartOfTitleCommand();
        }
    };

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
