package com.seareon.servlet.command.impl.topics.impl.work;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.entity.impl.Topic;
import com.seareon.entity.impl.TopicQuestion;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.topics.WorkWithTopics;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * The class implements the command to update the question.
 * @version 1.0
 * @author Misha Ro
 */
public class UpdateQuestionCommand extends WorkWithTopics {
    /**
     * The method updates the question.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(verify(request)) {
            AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
            Question question = (Question) abstractDAO.findEntityById(
                    Integer.parseInt(request.getParameter(Constant.ID)));

            question.setTitle(request.getParameter(Constant.HEADER));
            question.setText(request.getParameter(Constant.QUESTION_BODY));

            boolean newQuestion = checkQuestionTopics(request.getParameter(Constant.OLD_TOPICS),
                    request.getParameter(Constant.TOKENS), question.getId(), request);

            Question sessionObject = (Question) request.getSession().getAttribute(Constant.OBJECT);

            if (question != null && (!sessionObject.equals(question) || newQuestion)) {
                question.setNewEntity(true);
                abstractDAO.update(question, question.getId());
                request.getSession().setAttribute(Constant.OBJECT, question);
            }
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.FILL_IN_ALL_FIELDS);
        }
    }

    /**
     * The method checks the topics for the presence of new topics.
     * @param oldTopics - List of old topics.
     * @param newTopics - List of new topics.
     * @param questionId - Question id.
     * @param request - HttpServletRequest object.
     * @return - If there are new topics then true otherwise false.
     */
    private boolean checkQuestionTopics(String oldTopics, String newTopics, int questionId,
                                        HttpServletRequest request) {

        List<String> oldTopicList = collectionDifference(Arrays.asList(oldTopics.split("\\s+")),
            Arrays.asList(newTopics.split(" ")));
        List<String> newTopicList = collectionDifference(Arrays.asList(newTopics.split("\\s+")),
            Arrays.asList(oldTopics.split(" ")));

        if(!oldTopicList.equals(newTopicList)) {
            String[] removeTopics = createStringArray(oldTopicList);
            String[] addTopics = createStringArray(newTopicList);

            createTopics(addTopics, questionId, request);
            deleteTopicsRelationsFromQuestion(removeTopics, questionId, request);

            return true;
        }

        return false;
    }

    /**
     * The method calculates the difference of the lists.
     * @param a - List of String.
     * @param b - List of String.
     * @return - List of String.
     */
    private List<String> collectionDifference(List<String> a, List<String> b) {
        List<String> result = new ArrayList<>(a);
        result.removeAll(b);

        return result;
    }

    /**
     * The method converts a list of strings to an array of strings.
     * @param strings - List of String.
     * @return - Array of String.
     */
    private String[] createStringArray(List<String> strings) {
        String[] strs = new String[strings.size()];
        strs = strings.toArray(strs);
        return  strs;
    }

    /**
     * The method removes topics from the question.
     * @param removeTopics - Array of String
     * @param questionId - Question id.
     * @param request - HttpServletRequest object.
     */
    private void deleteTopicsRelationsFromQuestion(String[] removeTopics, int questionId, HttpServletRequest request) {
        AbstractDAO abstractDAOTopicQuestion = AbstractDAO.getInstance(Constant.TOPIC_QUESTION_DAO);
        AbstractDAO abstractTopicDAO = AbstractDAO.getInstance(Constant.TOPIC_DAO);

        Topic topicEntity = new Topic();
        TopicQuestion topicQuestion = new TopicQuestion();
        Map<Integer, Integer> id = new HashMap<>(1);
        List<Topic> topicList = (List<Topic>) request.getSession().getAttribute(Constant.TOPICS);

        for (String topic : removeTopics) {
            id.clear();

            topicEntity.setText(topic);
            abstractTopicDAO.isContainEntity(topicEntity);

            id.put(questionId, topicEntity.getId());
            topicQuestion.setId(id);

            topicList.remove(topicEntity);

            abstractDAOTopicQuestion.delete(topicQuestion);

            topicEntity.setCountQuestion(topicEntity.getCountQuestion() - Constant.ONE);
            if(topicEntity.getCountQuestion() == Constant.ZERO) {
                topicEntity.setActive(false);
                abstractTopicDAO.update(topicEntity, topicEntity.getId());
            }
        }
    }
}
