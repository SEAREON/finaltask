package com.seareon.servlet.command.impl;

import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Если команда не определена, то вызывается этот класс.
 * @version 1.0
 * @author Misha Ro
 */
public class WrongAction extends ActionCommand {
    private final String ERROR_CODE = "500";
    /**
     * Redirects to the error page with error code - 500.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().setAttribute(Constant.CURRENT_PAGE, ERROR_CODE);
    }
}
