package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the command to delete the user's avatar.
 * @version 1.0
 * @author Misha Ro
 */
public class DeleteImageCommand extends ActionCommand {
    /**
     * The method deletes the user's avatar.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter(Constant.ID));
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.IMAGE_DAO);

        if(!abstractDAO.delete(Integer.valueOf(id))) {
            response.sendError(500, Constant.DO_NOT_DELETE);
        } else if(request.getSession().getAttribute(Constant.USER) != null &&
                ((Account)request.getSession().getAttribute(Constant.USER)).getImage() == id) {

            ((Account)request.getSession().getAttribute(Constant.USER)).setImage(Constant.ZERO);
        }

    }
}
