package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Help;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * The class implements the command to create a help message.
 * @version 1.0
 * @author Misha Ro
 */
public class CreatingRequestForHelpCommand extends ActionCommand {
    /**
     * The method creates a new help message.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(verify(request)) {
            Help help = new Help();
            help.setTitle(request.getParameter(Constant.HEADER));
            help.setText(request.getParameter(Constant.HELP_BODY));
            help.setAccountId(Integer.parseInt(request.getParameter(Constant.OWNER)));
            help.setTime(new Date());
            help.setNewEntity(true);
            AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.HELP_DAO);
            if(abstractDAO.create(help) == null) {
                request.setAttribute(Constant.ERROR_MESSAGE, Constant.NOT_AUTHORISATION);
            }
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.FILL_IN_ALL_FIELDS);
        }
    }
}
