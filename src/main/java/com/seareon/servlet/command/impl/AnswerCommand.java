package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * The class implements the command to create a new answer.
 * @version 1.0
 * @author Misha Ro
 */
public class AnswerCommand extends ActionCommand {
    /**
     * The method creates a new answer.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(verify(request)) {
            Answer answer = new Answer();
            answer.setQuestionId(Integer.parseInt(request.getParameter(Constant.QUESTION_ID_JSP)));

            Account user = (Account) request.getSession().getAttribute(Constant.USER);

            if (user != null) {

                answer.setAccountId(user.getId());
                answer.setText(request.getParameter(Constant.POST_TEXT));
                answer.setTime(new Date());

                AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ANSWER_DAO);
                int id = (int) abstractDAO.create(answer);
                answer = (Answer) abstractDAO.findEntityById(id);

                ((List<Answer>) request.getSession().getAttribute("list1")).add(answer);

                Question question = (Question) request.getSession().getAttribute(Constant.OBJECT);
                question.setCountAnswers(question.getCountAnswers() + Constant.ONE);
                question.setNewAnswers(question.getCountAnswers() + Constant.ONE);

                addAccount(request, answer.getAccountId());
            } else {
                request.setAttribute(Constant.ERROR_MESSAGE, Constant.LOG_IN_AGAIN);
            }
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.ANSWER_DOES_NOT_CONTAIN_TEXT);
        }
    }

    /**
     * The method enters the account object in the list of accounts, if it is not there.
     * @param request - HttpServletRequest object.
     * @param id - Account id.
     */
    private void addAccount(HttpServletRequest request, int id) {
        List<Account> users = (List<Account>) request.getSession().getAttribute(Constant.ACCOUNTS);

        if(!checkUsers(users, id)) {
            AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
            Account account = (Account) abstractDAO.findEntityById(id);

            users.add(account);
        }
    }

    /**
     * The method checks the presence of an account in the list of accounts.
     * @param accounts - List of Accounts.
     * @param id - Account id.
     * @return
     */
    private boolean checkUsers(List<Account> accounts, int id) {
        for(Account account : accounts) {
            if(account.getId() == id) {
                return true;
            }
        }

        return false;
    }
}
