package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Help;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the command to view all the help messages.
 * @version 1.0
 * @author Misha Ro
 */
public class HelpCommand extends ActionCommand {
    /**
     * The method finds some Account for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter(Constant.ID));

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.HELP_DAO);
        Help help = (Help) abstractDAO.findEntityById(id);

        abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        Account account = (Account) abstractDAO.findEntityById(help.getAccountId());

        request.getSession().setAttribute(Constant.OBJECT, help);
        request.getSession().setAttribute(Constant.OBJECT_2, account);

        setCurrentPage(request, Constant.HELP_PAGE);
    }
}
