package com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.Topic;
import com.seareon.entity.impl.TopicQuestion;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.OperationWithTopics;
import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * The abstract class provides tools for working with Topic.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class TopicMenu extends OperationWithTopics {
    /**
     * The method checks the number of topics and if there are less than 10, then adds new ones from the database.
     * @param topics - List of Topic
     * @return - Set of Topic
     */
    protected Set<Topic> getTopicMenu(List<Topic> topics) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.TOPIC_DAO);
        Set<Topic> topicMenu = new LinkedHashSet<>(topics);

        if(topicMenu.size() < Constant.TEN) {
            topicMenu.addAll(abstractDAO.findEntitiesFromTo(Constant.ZERO, Constant.TEN));
        }

        return  topicMenu;
    }

    /**
     * The method enters all the topics in the session attribute.
     * @param request - HttpServletRequest object.
     * @param questions - List of Questions.
     */
    protected void setTopicMenu(HttpServletRequest request, List<Question> questions) {
        List<TopicQuestion> topicQuestions = getTopicQuestion(questions);
        List<Topic> topics = getTopicsFromTopicQuestion(topicQuestions);
        Set<Topic> topicMenu = getTopicMenu(topics);
        List<Account> accounts = getOwnersForEntities(questions);

        setNecessaryData(request, questions, topicQuestions, topics, accounts);
        request.getSession().setAttribute(Constant.TOPIC_MENU, topicMenu);
    }
}
