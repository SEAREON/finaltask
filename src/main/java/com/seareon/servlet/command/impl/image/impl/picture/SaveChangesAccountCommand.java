package com.seareon.servlet.command.impl.image.impl.picture;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.resource.Util;
import com.seareon.servlet.Controller;
import com.seareon.servlet.command.impl.image.Picture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the user profile update command.
 * @version 1.0
 * @author Misha Ro
 */
public class SaveChangesAccountCommand extends Picture {
    /**
     * The method updates the user profile.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Account account = new Account();
        account.setLogin(request.getParameter(Constant.LOGIN_EDIT_ACCOUNT));
        account.setPassword(request.getParameter(Constant.PASSWORD_EDIT_ACCOUNT));

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        abstractDAO.isContainEntity(account);

        account.setLogin(request.getParameter(Constant.LOGIN_EDIT).trim());
        account.setDescription(request.getParameter(Constant.AREA_DESCRIPTION).trim());
        account.setEmail(request.getParameter(Constant.EMAIL_USER_EDIT).trim());

        loadPicture(request, account);

        Account userEdit = (Account) request.getSession().getAttribute(Constant.USER_EDIT);

        if(!userEdit.equals(account)) {
            if (abstractDAO.update(account, userEdit.getId()/*account.getId()*/) != null) {
                account.setNewEntity(true);
                request.getSession().setAttribute(Constant.USER_EDIT, account);

                if(((Account)request.getSession().getAttribute(Constant.USER)).getId() == account.getId()) {
                    request.getSession().setAttribute(Constant.USER, account);
                }
            } else {
                request.setAttribute(Constant.ERROR_MESSAGE, Constant.SAVE_ACCOUNT_ERROR);
            }
        }
    }

    /**
     * The method checks the correctness of the parameters in the client request.
     * @param request - HttpServletRequest object.
     * @return - If all query parameters are correct then true otherwise false.
     */
    @Override
    protected boolean verify(HttpServletRequest request) {
        String login = request.getParameter(Constant.LOGIN_EDIT);
        String email = request.getParameter(Constant.EMAIL_USER_EDIT);

        if(!login.isEmpty() && Util.match(login, Constant.LOGIN_ACCOUNT_EDIT_REGEX) &&
                Util.match(email, Constant.EMAIL_REGEX)) {
            return true;
        }

        return false;
    }
}
