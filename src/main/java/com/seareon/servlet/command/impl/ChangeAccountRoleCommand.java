package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the command to change the user role.
 * @version 1.0
 * @author Misha Ro
 */
public class ChangeAccountRoleCommand extends ActionCommand {
    /**
     * The method changes the role of the user to the opposite.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter(Constant.ID));

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        Account account = (Account) abstractDAO.findEntityById(id);

        account.setRole(account.isRole() ? false : true);

        abstractDAO.update(account, account.getId());
    }
}
