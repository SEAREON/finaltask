package com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.impl.topic.menu;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.TopicQuestionDAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.entity.impl.TopicQuestion;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.TopicMenu;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The class implements the command of finding all the questions on the topic.
 * @version 1.0
 * @author Misha Ro
 */
public class QuestionByTopicCommand extends TopicMenu {
    /**
     * The method is to find all the questions with the same theme and put them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.TOPIC_QUESTION_DAO);

        List<TopicQuestion> topicQuestions =
                ((TopicQuestionDAO)abstractDAO).findEntitiesByTopicId(Integer.parseInt(
                        request.getParameter(Constant.ID)));

        List<Question> questions = getQuestionsByTopicQuestion(topicQuestions);

        int countPages = (int) Math.ceil(questions.size() / Constant.DOUBLE_TEN);

        setTopicMenu(request, questions);
        setPagesCounter(request, Constant.ONE, countPages);

        setCurrentPage(request, Constant.QUESTION_PAGE);
    }

    /**
     * The method is to find all the questions with the same themes.
     * @param topicQuestions - List of TopicQuestion.
     * @return - List of Question.
     */
    private List<Question> getQuestionsByTopicQuestion(List<TopicQuestion> topicQuestions) {
        List<Question> questions = new ArrayList<>();
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

        int questionId;
        Question question;

        for(TopicQuestion topicQuestion : topicQuestions) {
            questionId = topicQuestion.getId().keySet().iterator().next();
            question = (Question) abstractDAO.findEntityById(questionId);

            if(question.isActive()) {
                questions.add(question);
            }
        }

        if(questions.size() > Constant.TEN) {
            questions = questions.subList(Constant.ZERO, Constant.TEN);
        }

        return  questions;
    }
}
