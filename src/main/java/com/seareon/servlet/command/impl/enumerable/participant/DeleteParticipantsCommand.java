package com.seareon.servlet.command.impl.enumerable.participant;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.AccountDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.Page;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The class implements the command to view all deleted users.
 * @version 1.0
 * @author Misha Ro
 */
public class DeleteParticipantsCommand extends Page {
    /**
     * The method finds all deleted users for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int currentPage = Integer.parseInt(request.getParameter(Constant.CURRENT_PAGE));

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        double countPages = ((AccountDAO)abstractDAO).countDeleteEntities();

        List<Account> accounts =
                ((AccountDAO)abstractDAO).findDeleteAccountFromTo((currentPage - Constant.ONE) * Constant.TWELVE,
                        currentPage * Constant.TWELVE);

        request.setAttribute(Constant.ACCOUNTS, accounts);
        setPagesCounter(request, currentPage, (int) Math.ceil(countPages / Constant.TWELVE));
        setPageCommand(request);

        setCurrentPage(request, Constant.PARTICIPANTS_PAGE);
    }
}
