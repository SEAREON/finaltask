package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the command to edit the user profile.
 * @version 1.0
 * @author Misha Ro
 */
public class EditAccountCommand extends ActionCommand {
    /**
     *
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        int id = Integer.parseInt(request.getParameter(Constant.USER_ID));
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        Account account = (Account) abstractDAO.findEntityById(id);

        request.getSession().setAttribute(Constant.USER_EDIT, account);

        setCurrentPage(request, Constant.EDIT_ACCOUNT_PAGE);
    }
}
