package com.seareon.servlet.command.impl.verifying.impl.verifying;

import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.verifying.VerifyCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the Question verification command.
 * @version 1.0
 * @author Misha Ro
 */
public class QuestionVerifiedCommand extends VerifyCommand<Question> {
    /**
     * The method marks Question as verified.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        verify(request, Constant.QUESTION_DAO);
    }
}
