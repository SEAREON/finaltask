package com.seareon.servlet.command.impl.enumerable.owner.help;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Help;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.OperationWithTopics;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The class implements the command to view all help messages.
 * @version 1.0
 * @author Misha Ro
 */
public class SearchHelpCommand extends OperationWithTopics {
    /**
     * The method finds all help messages for this page and places them in the session attribute.
     * request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.HELP_DAO);

        int countPages = (int) Math.ceil(abstractDAO.countEntities() / Constant.DOUBLE_TEN);
        int currentPage = Integer.parseInt(request.getParameter(Constant.CURRENT_PAGE));
        List<Help> helps = abstractDAO.findEntitiesFromTo(Constant.TEN * (currentPage - Constant.ONE),
                currentPage * Constant.TEN);
        List<Account> accounts = getOwnersForQuestions(helps);

        request.getSession().setAttribute(Constant.LIST_1, helps);
        request.getSession().setAttribute(Constant.ACCOUNTS, accounts);
        setPagesCounter(request, currentPage, countPages);
        setPageCommand(request);
        setCurrentPage(request, Constant.HELP_ADMIN_PAGE);
    }

    /**
     * The method returns a list of owners of help messages.
     * @param helps - List of Helps.
     * @return - List of Account.
     */
    private  List<Account> getOwnersForQuestions(List<Help> helps) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        List<Account> accounts = new ArrayList<>(helps.size());

        for(Help help : helps) {
            Account account = (Account) abstractDAO.findEntityById(help.getAccountId());

            if(!accounts.contains(account)) {
                accounts.add(account);
            }
        }

        return accounts;
    }
}
