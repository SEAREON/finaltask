package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.Assessment;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * The class implements the work with the Account rating.
 * @version 1.0
 * @author Misha Ro
 */
public class RatingCommand extends ActionCommand {
    /**
     * The method performs operations with the response rating and the account to which the response belongs.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        if(request.getSession().getAttribute(Constant.USER) != null) {

            Answer answer = getAnswer(request);
            Assessment assessment = newAssessment(request, answer);

            doActionWithRating(request, answer, assessment);
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.NOT_AUTHORISATION);
        }

        setCurrentPage(request, Constant.ANSWER_QUESTION_PAGE);
    }

    /**
     * The method receives the object of the response over which the operation is performed.
     * @param request - HttpServletRequest object.
     * @return - The answer over which the operation was performed.
     */
    private Answer getAnswer(HttpServletRequest request) {

        int id = Integer.parseInt(request.getParameter(Constant.ID));
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ANSWER_DAO);
        Answer answer = (Answer) abstractDAO.findEntityById(id);

        return answer;
    }

    /**
     * The method sets the connection between the user and the answer for which he voted.
     * @param request - HttpServletRequest object.
     * @param answer - Answer object.
     * @return
     */
    private Assessment newAssessment(HttpServletRequest request, Answer answer) {

        Assessment assessment = new Assessment();
        HashMap<Integer, Integer> map = new HashMap<>(Constant.ONE);
        map.put(answer.getId(), ((Account)request.getSession().getAttribute(Constant.USER)).getId());
        assessment.setId(map);

        return assessment;
    }

    /**
     * The method checks whether the user voted for this answer.
     * @param request - HttpServletRequest object.
     * @param answer - Answer object.
     * @param assessment - Assessment object.
     */
    private void doActionWithRating(HttpServletRequest request, Answer answer, Assessment assessment) {

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ASSESSMENT_DAO);

        if(!abstractDAO.isContainEntity(assessment)) {

            abstractDAO.create(assessment);

            Answer answerFromList = null;
            Iterator<Answer> it = ((List<Answer>) request.getSession().getAttribute("list1")).iterator();
            while (it.hasNext()) {
                answerFromList = it.next();
                if (answer.getId() == answerFromList.getId()) {
                    upOrDownRating(request, answerFromList);
                    break;
                }
            }

            abstractDAO = AbstractDAO.getInstance(Constant.ANSWER_DAO);
            abstractDAO.update(answerFromList, answer.getId());
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.ALREADY_RATED);
        }
    }

    /**
     * The method changes the rating of the answer and the user who wrote it.
     * @param request - HttpServletRequest object.
     * @param answer - Answer object.
     */
    private void upOrDownRating(HttpServletRequest request, Answer answer) {

        Account owner = getQuestionOwner(request, answer);

        if(request.getParameter("action").equals("up")) {
            answer.setMark(answer.getMark() + Constant.ONE);
            owner.setRating(owner.getRating() + Constant.ONE);
        } else {
            answer.setMark(answer.getMark() - Constant.ONE);
            owner.setRating(owner.getRating() - Constant.ONE);
        }
    }

    /**
     * The method finds the user who asked the current question.
     * @param request - HttpServletRequest object.
     * @param answer - Answer object.
     * @return - Account object.
     */
    private Account getQuestionOwner(HttpServletRequest request, Answer answer) {
        Account owner = null;
        Iterator<Account> it = ((List<Account>) request.getSession().getAttribute("accounts")).iterator();

        while (it.hasNext()) {
            owner = it.next();
            if(owner.getId() == answer.getAccountId()) {
                break;
            }
        }

        return owner;
    }
}
