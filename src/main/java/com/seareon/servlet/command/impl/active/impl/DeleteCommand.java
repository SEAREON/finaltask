package com.seareon.servlet.command.impl.active.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.Entity;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import com.seareon.servlet.command.impl.active.ActiveCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Misha Ro on 28.04.2017.
 */
public class DeleteCommand extends ActiveCommand {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String daoType = request.getParameter(Constant.ENTITY);
        int id = Integer.parseInt(request.getParameter(Constant.ID));

        setActive(daoType, id);

        doOperationWithEntity(request, id, daoType);
    }

    @Override
    protected void setActive(String daoType, int id) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(daoType + Constant.DAO);
        abstractDAO.delete(id);
    }

    @Override
    protected void setQuestionPropertyWhenOperationWithAnswer(HttpServletRequest request, Answer answer) {
        Question question = (Question) request.getSession().getAttribute(Constant.OBJECT);

        question.setCountAnswers(question.getCountAnswers() - Constant.ONE);
        question.setDeleteAnswers(question.getDeleteAnswers() + Constant.ONE);

        if(answer.isNewEntity()) {
            question.setNewAnswers(question.getNewAnswers() - Constant.ONE);
        }

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        abstractDAO.update(question, question.getId());
    }

    @Override
    protected void setQuestionPropertyWhenOperationWithQuestion(HttpServletRequest request, List<Answer> answers) {
        Question question = (Question) request.getSession().getAttribute(Constant.OBJECT);

        question.setActive(false);
        question.setDeleteAnswers(answers.size());
        question.setNewAnswers(Constant.ZERO);
        question.setCountAnswers(Constant.ZERO);

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        abstractDAO.update(question, question.getId());
    }

    @Override
    protected void setAccountActive(HttpServletRequest request) {
        Account account = (Account) request.getSession().getAttribute(Constant.USER_EDIT);

        if(((Account)request.getSession().getAttribute(Constant.USER)).getId() == account.getId()) {
            request.getSession().removeAttribute(Constant.USER);
        }
    }
}
