package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Implementation of the answer update command.
 * @version 1.0
 * @author Misha Ro
 */
public class UpdateAnswerCommand extends ActionCommand {
    /**
     * Method calls for updating the response record in the database.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(verify(request)) {
            AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ANSWER_DAO);
            Answer answer = (Answer) abstractDAO.findEntityById(Integer.parseInt(request.getParameter(Constant.ID)));
            String answerText = request.getParameter(Constant.ANSWER_BODY);

            if (!answer.getText().equals(answerText)) {
                answer.setText(answerText);

                if (abstractDAO.update(answer, answer.getId()) != null) {
                    List<Answer> answers = (List<Answer>) request.getSession().getAttribute(Constant.LIST_1);
                    updateAnswersIntoPage(answers, answer);
                } else {
                    request.setAttribute(Constant.ERROR_MESSAGE, Constant.ERROR_ANSWER_UPDATE);
                }
            }
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.FILL_IN_ALL_FIELDS);
        }
    }

    /**
     * The method updates the response in the response list to display on the page.
     * @param answers - List of answers.
     * @param answer - Updated answer.
     */
    private void updateAnswersIntoPage(List<Answer> answers, Answer answer) {
        for(int answerIndex = 0; answerIndex < answers.size(); answerIndex++) {
            if(answers.get(answerIndex).getId() == answer.getId()) {
                answers.set(answerIndex, answer);
                answer.setNewEntity(true);
            }
        }
    }
}
