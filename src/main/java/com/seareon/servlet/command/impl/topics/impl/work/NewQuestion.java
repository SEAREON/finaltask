package com.seareon.servlet.command.impl.topics.impl.work;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.topics.WorkWithTopics;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * The class implements the command to create a new question.
 * @version 1.0
 * @author Misha Ro
 */
public class NewQuestion extends WorkWithTopics {
    /**
     * The method creates a new question.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (verify(request)) {
            String header = request.getParameter(Constant.HEADER);
            Question question = new Question();
            question.setTitle(header);

            AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

            if (!abstractDAO.isContainEntity(question)) {
                question.setText(request.getParameter(Constant.QUESTION_BODY));
                int id = ((Account) request.getSession().getAttribute(Constant.USER)).getId();
                question.setAccountId(id);
                question.setTime(new Date());
                int questionId = (int) abstractDAO.create(question);
                if (questionId != Constant.MINUS_ONE) {
                    createTopics(request.getParameter(Constant.TOKENS).split(" "), questionId, request);
                    setCurrentPage(request, Constant.INDEX_PAGE);

                    return;
                } else {
                    request.setAttribute(Constant.ERROR_MESSAGE, Constant.QUESTION_DOES_NOT_CREATE);
                }
            } else {
                request.setAttribute(Constant.ERROR_MESSAGE, Constant.QUESTION_EXISTS);
            }

            setCurrentPage(request, "path.page.askQuestion");
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.FILL_IN_ALL_FIELDS);
        }
    }
}
