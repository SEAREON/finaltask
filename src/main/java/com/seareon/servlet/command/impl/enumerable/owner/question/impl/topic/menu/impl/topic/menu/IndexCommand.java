package com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.impl.topic.menu;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.TopicMenu;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The class implements the command to view the start page.
 * @version 1.0
 * @author Misha Ro
 */
public class IndexCommand extends TopicMenu {
    /**
     * The method finds all question for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

        int countPages = (int)Math.ceil(abstractDAO.countEntities() / Constant.DOUBLE_TEN);
        int currentPage = getCurrentPage(request);

        List questions = abstractDAO.findEntitiesFromTo(Constant.TEN * (currentPage - Constant.ONE),
                currentPage * Constant.TEN);

        setTopicMenu(request, questions);
        setPagesCounter(request, currentPage, countPages);
        setPageCommand(request);
        setCurrentPage(request, Constant.QUESTION_PAGE);
    }

    private int getCurrentPage(HttpServletRequest request) {
        String page = request.getParameter(Constant.CURRENT_PAGE);
        return page == null ? Constant.ONE : Integer.parseInt(page);
    }
}
