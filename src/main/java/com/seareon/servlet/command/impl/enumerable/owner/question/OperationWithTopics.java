package com.seareon.servlet.command.impl.enumerable.owner.question;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.TopicQuestionDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.Topic;
import com.seareon.entity.impl.TopicQuestion;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.Owner;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * The abstract class implements operations with topics.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class OperationWithTopics extends Owner {
    /**
     * The method from TopicQuestion objects takes out the Topic objects.
     * @param topicQuestions - List of TopicQuestion.
     * @return - List of Topic.
     */
    protected List<Topic> getTopicsFromTopicQuestion(List<TopicQuestion> topicQuestions) {
        List<Topic> topics = new ArrayList<>(topicQuestions.size());
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.TOPIC_DAO);
        Topic temp;

        for(TopicQuestion topicQuestion : topicQuestions) {
            Iterator<Integer> it = topicQuestion.getId().keySet().iterator();
            int topic_id = topicQuestion.getId().get(it.next());
            temp = (Topic) abstractDAO.findEntityById(topic_id);

            if(!topics.contains(temp)) {
                topics.add(temp);
            }
        }

        return topics;
    }

    /**
     * The method from the Question objects takes out the TopicQuestion objects.
     * @param questions - List of Questions.
     * @return - List of TopicQuestions.
     */
    protected List<TopicQuestion> getTopicQuestion(List<Question> questions) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.TOPIC_QUESTION_DAO);
        List<TopicQuestion> topicQuestions = new ArrayList<>();

        for (Question question : questions) {
            topicQuestions.addAll(
                    (List<TopicQuestion>) ((TopicQuestionDAO)abstractDAO).findEntitiesByQuestionId(question.getId()));
        }

        return  topicQuestions;
    }

    /**
     * The method enters parameters into the session as attributes.
     * @param request - HttpServletRequest object.
     * @param questions - List of Questions.
     * @param topicQuestions - List of TopicQuestions.
     * @param topics - List of Topics.
     * @param accounts - List of Accounts.
     */
    protected void setNecessaryData(HttpServletRequest request, List<Question> questions,
                                    List<TopicQuestion> topicQuestions, List<Topic> topics,
                                    List<Account> accounts) {

        request.getSession().setAttribute(Constant.LIST_1, questions);
        request.getSession().setAttribute(Constant.LIST_2, topicQuestions);
        request.getSession().setAttribute(Constant.TOPICS, topics);
        request.getSession().setAttribute(Constant.ACCOUNTS, accounts);
    }
}
