package com.seareon.servlet.command.impl.image.impl.picture;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.resource.Util;
import com.seareon.servlet.command.impl.image.Picture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the user registration command.
 * @version 1.0
 * @author Misha Ro
 */
public class RegistrationCommand extends Picture {
    /**
     * The method creates a new user.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (verify(request)) {
            Account account = new Account();

            account.setLogin(request.getParameter(Constant.DISPLAY_LOGIN));
            account.setPassword(request.getParameter(Constant.DISPLAY_PASSWORD));
            account.setEmail(request.getParameter(Constant.EMAIL));
            account.setDescription(request.getParameter(Constant.DESCRIPTION));

            AbstractDAO abstractDAO;

            loadPicture(request, account);

            abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);

            try {
                account.setId((Integer) abstractDAO.create(account));

                if (account.getId() != null) {
                    request.getSession().setAttribute(Constant.USER, account);
                } else {
                    request.setAttribute(Constant.ERROR_MESSAGE, Constant.COULD_NOT_CREATE_ACCOUNT);
                    setCurrentPage(request, Constant.REGISTRATION_PAGE);
                }
            } catch (Exception e) {
                request.setAttribute(Constant.ERROR_MESSAGE, Constant.USER_WITH_SUCH_EMAIL_EXISTS);
                setCurrentPage(request, Constant.REGISTRATION_PAGE);
            }
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.FILL_IN_ALL_FIELDS_WITH_REQUIREMENTS);
        }
    }

    /**
     * The method checks the correctness of the parameters in the client request.
     * @param request - HttpServletRequest object.
     * @return - If all query parameters are correct then true otherwise false.
     */
    @Override
    protected boolean verify(HttpServletRequest request) {
        String login = request.getParameter(Constant.DISPLAY_LOGIN);
        String password = request.getParameter(Constant.DISPLAY_PASSWORD);
        String email = request.getParameter(Constant.EMAIL);

        if(Util.match(login, Constant.LOGIN_REGEX) && Util.match(password, Constant.PASSWORD_REGEX) &&
                Util.match(email, Constant.EMAIL_REGEX)) {
            return true;
        }

        return false;
    }
}
