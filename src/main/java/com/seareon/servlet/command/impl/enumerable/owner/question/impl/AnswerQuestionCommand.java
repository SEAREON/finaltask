package com.seareon.servlet.command.impl.enumerable.owner.question.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.AnswerDAO;
import com.seareon.dao.impl.TopicQuestionDAO;
import com.seareon.entity.impl.*;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.OperationWithTopics;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * The class implements the command to view the question and its answers.
 * @version 1.0
 * @author Misha Ro
 */
public class AnswerQuestionCommand extends OperationWithTopics {
    /**
     * The method finds the question and its answers for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        AbstractDAO questionDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        AbstractDAO topicQuestionDAO = AbstractDAO.getInstance(Constant.TOPIC_QUESTION_DAO);
        AbstractDAO answerDAO = AbstractDAO.getInstance(Constant.ANSWER_DAO);

        int id = Integer.parseInt(request.getParameter(Constant.ID));
        Question question = (Question) questionDAO.findEntityById(id);

        List<TopicQuestion> topicQuestions = (List<TopicQuestion>)
                ((TopicQuestionDAO)topicQuestionDAO).findEntitiesByQuestionId(question.getId());

        List<Topic> topics = getTopicsFromTopicQuestion(topicQuestions);

        List<Answer> answers = ((AnswerDAO)answerDAO).findEntitiesByQuestionId(id);
        Collections.sort(answers);

        List<Account> accounts = getOwnersForQuestionsAndAnswers(answers, question.getAccountId());

        Set<Question> likeQuestions = getLikeQuestions(topics, question);

        request.getSession().setAttribute(Constant.OBJECT, question);
        request.getSession().setAttribute(Constant.TOPICS, topics);
        request.getSession().setAttribute(Constant.LIST_1, answers);
        request.getSession().setAttribute(Constant.ACCOUNTS, accounts);
        request.getSession().setAttribute(Constant.LIST_2, likeQuestions);

        setCurrentPage(request, Constant.ANSWER_QUESTION_PAGE);
    }

    /**
     * The method looks for owners for question and answer.
     * @param answers - List of Answers.
     * @param questionId - Question id.
     * @return - List of Answers.
     */
    private  List<Account> getOwnersForQuestionsAndAnswers(List<Answer> answers, int questionId) {
        List<Account> accounts = new ArrayList<>(answers.size() + Constant.ONE);
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);

        accounts.add((Account) abstractDAO.findEntityById(questionId));
        accounts.addAll(getOwnersForEntities(answers));

        return accounts;
    }

    /**
     * The method looks for answers with the same topics.
     * @param topics - List of Topics.
     * @param question - Question object.
     * @return - Set of Question.
     */
    private Set<Question> getLikeQuestions(List<Topic> topics, Question question) {
        Set<Question> likeQuestions = new LinkedHashSet<>();
        AbstractDAO questionDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        AbstractDAO topicQuestionDAO = AbstractDAO.getInstance(Constant.TOPIC_QUESTION_DAO);
        List<TopicQuestion> topicQuestions;

        for(Topic topic : topics) {
            topicQuestions = ((TopicQuestionDAO) topicQuestionDAO).findEntitiesByTopicId(topic.getId());

            for(TopicQuestion topicQuestion : topicQuestions) {
                Iterator<Integer> it = topicQuestion.getId().keySet().iterator();
                int question_id = it.next();
                likeQuestions.add((Question)questionDAO.findEntityById(question_id));
            }
        }

        likeQuestions.remove(question);

        return likeQuestions;
    }
}
