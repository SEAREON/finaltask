package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.resource.MessageManager;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the user's authorization command.
 * @version 1.0
 * @author Misha Ro
 */
public class LoginCommand extends ActionCommand {
    final private String error = "errorLogPassMessage";

    /**
     * Account login
     */
    private final String PARAM_LOGIN = "login";

    /**
     * Account password
     */
    private final String PARAM_PASSWORD = "password";

    /**
     * The method authorizes the user and adds it as an attribute to the session.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String login = request.getParameter(PARAM_LOGIN);
            String password = request.getParameter(PARAM_PASSWORD);

            AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);

            Account account = new Account();
            account.setLogin(login);
            account.setPassword(password);

            if (abstractDAO.isContainEntity(account)) {
                request.getSession().setAttribute(Constant.USER, account);
                request.setAttribute(Constant.SHOW, null);
            } else {
                request.setAttribute(error, Constant.INCORRECT_LOGIN_OR_PASSWORD);

                request.setAttribute(Constant.SHOW, Constant.LOGIN);
            }
    }
}
