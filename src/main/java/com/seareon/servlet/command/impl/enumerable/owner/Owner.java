package com.seareon.servlet.command.impl.enumerable.owner;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.AccountLink;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.Page;
import java.util.ArrayList;
import java.util.List;

/**
 * The abstract class implements the command to search for the owners of some entities.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class Owner extends Page {
    /**
     * The method finds Account that created entities.
     * @param entities - A subclass of the entity class.
     * @param <T> - Defines the data type for Entity.
     * @return - List of Account.
     */
    protected <T extends AccountLink> List<Account> getOwnersForEntities(List<T> entities) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ACCOUNT_DAO);
        List<Account> accounts = new ArrayList<>(entities.size());

        for(T entity : entities) {
            Account account = (Account) abstractDAO.findEntityById(entity.getAccountId());

            if(!accounts.contains(account)) {
                accounts.add(account);
            }
        }

        return accounts;
    }
}
