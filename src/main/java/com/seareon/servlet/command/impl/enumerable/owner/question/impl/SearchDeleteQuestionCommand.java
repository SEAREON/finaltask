package com.seareon.servlet.command.impl.enumerable.owner.question.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.QuestionQAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.entity.impl.Topic;
import com.seareon.entity.impl.TopicQuestion;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.OperationWithTopics;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The class implements the command to view all delete questions.
 * @version 1.0
 * @author Misha Ro
 */
public class SearchDeleteQuestionCommand extends OperationWithTopics {
    /**
     * The method finds all delete question for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

        int countPages = (int) Math.ceil(((QuestionQAO)abstractDAO).countNotActiveEntities() / Constant.DOUBLE_TEN);
        int currentPage = Integer.parseInt(request.getParameter("currentPage"));
        List<Question> questions =
                ((QuestionQAO)abstractDAO).findDeleteEntitiesFromTo(Constant.TEN * (currentPage - Constant.ONE),
                        currentPage * Constant.TEN);
        List<TopicQuestion> topicQuestions = getTopicQuestion(questions);
        List<Topic> topics = getTopicsFromTopicQuestion(topicQuestions);
        List<Account> accounts = getOwnersForEntities(questions);

        setNecessaryData(request, questions, topicQuestions, topics, accounts);
        setPagesCounter(request, currentPage, countPages);
        setPageCommand(request);

        setCurrentPage(request, Constant.QUESTION_ANSWER_ADMIN_PAGE);
    }
}
