package com.seareon.servlet.command.impl.image;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.Image;
import com.seareon.resource.Constant;
import com.seareon.servlet.Controller;
import com.seareon.servlet.command.ActionCommand;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

/**
 * Abstract class for working with a picture.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class Picture extends ActionCommand {
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * The method loads a new picture.
     * @param request - HttpServletRequest object.
     * @param account - Account object.
     * @throws IOException
     */
    protected void loadPicture(HttpServletRequest request, Account account) throws IOException {
        try {
            Part filePart = request.getPart(Constant.UPLOAD);
            StringBuilder fileName =
                    new StringBuilder(Paths.get(filePart.getSubmittedFileName()).getFileName().toString());
            if(fileName.length() > 0) {
                InputStream fileContent = filePart.getInputStream();
                Image image = new Image(fileContent, fileName.toString());

                AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.IMAGE_DAO);

                if(account.getImage() > 0) {
                    abstractDAO.delete(account.getImage());
                }

                account.setImage((Integer) abstractDAO.create(image));
            }
        } catch (ServletException e) {
            logger.error(request.getSession().getId() + Constant.ERROR_READING_IMAGE);
        }
    }
}
