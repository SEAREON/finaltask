package com.seareon.servlet.command.impl;

import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The class implements the exit command from the account.
 * @version 1.0
 * @author Misha Ro
 */
public class LogoutCommand extends ActionCommand {
    /**
     * The method deletes the user attribute from the session.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().removeAttribute(Constant.USER);
    }
}
