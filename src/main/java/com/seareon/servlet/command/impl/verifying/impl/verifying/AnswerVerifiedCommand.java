package com.seareon.servlet.command.impl.verifying.impl.verifying;

import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.verifying.VerifyCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the Answer verification command.
 * @version 1.0
 * @author Misha Ro
 */
public class AnswerVerifiedCommand extends VerifyCommand<Answer> {
    /**
     * The method marks Answer as verified.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        verify(request, Constant.ANSWER_DAO);
    }
}
