package com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.impl.topic.menu;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.QuestionQAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.TopicMenu;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The class implements the command to view all questions with new answers.
 * @version 1.0
 * @author Misha Ro
 */
public class SearchQuestionWithNewAnswersCommand extends TopicMenu {
    /**
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

        int countPages = (int) Math.ceil(
                ((QuestionQAO)abstractDAO).countQuestionsWithNewAnswers() / Constant.DOUBLE_TEN);
        int currentPage = Integer.parseInt(request.getParameter(Constant.CURRENT_PAGE));
        List<Question> questions =
                ((QuestionQAO)abstractDAO).findQuestionsWithNewAnswers(Constant.TEN * (currentPage - Constant.ONE),
                        currentPage * Constant.TEN);

        setTopicMenu(request, questions);
        setPagesCounter(request, currentPage, countPages);
        setPageCommand(request);
        setCurrentPage(request, Constant.QUESTION_ANSWER_ADMIN_PAGE);
    }
}
