package com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.impl.topic.menu;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.QuestionQAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.TopicMenu;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The class implements the command to find the question by part of the title.
 * @version 1.0
 * @author Misha Ro
 */
public class QuestionByPartOfTitleCommand extends TopicMenu {
    /**
     * The method finds all the questions where the title part is contained.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String partOfTitle = Constant.PERCENT + request.getParameter(Constant.PART_OF_TITLE) + Constant.PERCENT;

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        List<Question> questions = ((QuestionQAO)abstractDAO).findQuestionByPartOfTitle(partOfTitle);

        int currentPage = Integer.parseInt(request.getParameter(Constant.CURRENT_PAGE));
        int countPages = (int) Math.ceil(questions.size() / Constant.DOUBLE_TEN);

        setTopicMenu(request, chooseQuestions(questions, Constant.TEN * (currentPage - Constant.ONE)));
        setPagesCounter(request, currentPage, countPages);

        setCurrentPage(request, Constant.QUESTION_PAGE);
    }

    /**
     * The method returns a subList of questions.
     * @param questions - List of Question.
     * @param from - Number of questions.
     * @return - List of Question.
     */
    private  List<Question> chooseQuestions(List<Question> questions, int from) {
        int length = questions.size() - from > Constant.TEN ? Constant.TEN : questions.size() - from;
        return questions.subList(from, length);
    }
}
