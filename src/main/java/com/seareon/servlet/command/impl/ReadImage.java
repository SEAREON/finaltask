package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Image;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * The class represents the implementation of the command to send an image to the client.
 * @version 1.0
 * @author Misha Ro
 */
public class ReadImage extends ActionCommand {
    private final String IMAGE = "image";
    private final String IMAGE_FOR_COMTENT_TYPE = "image/";
    /**
     * The method reads a picture from the database and sends it by bytes to the client.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter(IMAGE);

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.IMAGE_DAO);
        Image image = (Image) abstractDAO.findEntityById(Integer.parseInt(id));

        response.setContentType(IMAGE_FOR_COMTENT_TYPE + image.getType());

        OutputStream writer = response.getOutputStream();
        writer.write(image.getImage());
        writer.flush();
    }
}
