package com.seareon.servlet.command.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Topic;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The class implements the command to view all topics.
 * @version 1.0
 * @author Misha Ro
 */
public class TopicsCommand extends ActionCommand {
    /**
     * The method finds all the topics for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int currentPage = Integer.parseInt(request.getParameter(Constant.CURRENT_PAGE));

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.TOPIC_DAO);
        double countPages = abstractDAO.countEntities();

        List<Topic> topics = abstractDAO.findEntitiesFromTo((currentPage - 1) * 65, currentPage * 65);

        request.getSession().setAttribute(Constant.TOPICS, topics);
        request.setAttribute(Constant.CURRENT_PAGE_NUMBER, currentPage);
        request.setAttribute(Constant.COUNT_PAGES, Math.ceil(countPages / 65));

        setCurrentPage(request, Constant.TOPICS_PAGE);
    }
}
