package com.seareon.servlet.command.impl;

import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * The class implements the command of internationalization.
 * @version 1.0
 * @author Misha Ro
 */
public class LanguageCommand extends ActionCommand {
    /**
     * The method changes the language in and saves it in the session as an attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        String locale = request.getParameter(Constant.LANGUAGE);
        Locale.setDefault(new Locale(locale));
        request.getSession().setAttribute(Constant.LOCALE, locale);
    }
}
