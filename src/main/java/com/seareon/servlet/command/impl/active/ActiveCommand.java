package com.seareon.servlet.command.impl.active;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Misha Ro on 03.05.2017.
 */
public abstract class ActiveCommand extends ActionCommand {
    protected abstract void setActive(String daoType,  int id);
    protected abstract void setQuestionPropertyWhenOperationWithAnswer(HttpServletRequest request, Answer answer);
    protected abstract void setQuestionPropertyWhenOperationWithQuestion(HttpServletRequest request, List<Answer> answers);
    protected abstract void setAccountActive(HttpServletRequest request);

    protected void doOperationWithEntity(HttpServletRequest request, int id, String entity) {
        switch (entity) {
            case Constant.ANSWER:
                Answer answer = getAnswer(id, entity);
                setQuestionPropertyWhenOperationWithAnswer(request,  answer);
                break;
            case Constant.QUESTION:
                List<Answer> answers = (List<Answer>) request.getSession().getAttribute(Constant.LIST_1);
                setQuestionPropertyWhenOperationWithQuestion(request, answers);
                break;
            case Constant.ACCOUNT:
                setAccountActive(request);
        }
    }

    private Answer getAnswer(int id, String entity) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(entity + Constant.DAO);
        Answer answer = (Answer) abstractDAO.findEntityById(id);

        return  answer;
    }
}
