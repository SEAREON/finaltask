package com.seareon.servlet.command.impl.verifying;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.Entity;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;

/**
 * The abstract class provides a verification interface.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class VerifyCommand <T extends Entity> extends ActionCommand {
    /**
     * The method marks some entity as verified.
     * @param request - HttpServletRequest object.
     * @param daoName - DAO level name.
     * @return
     */
    protected T verify(HttpServletRequest request, String daoName) {
        int id = Integer.parseInt(request.getParameter(Constant.ID));

        AbstractDAO abstractDAO = AbstractDAO.getInstance(daoName);

        T entity = (T) abstractDAO.findEntityById(id);
        entity.setNewEntity(false);
        abstractDAO.update(entity, entity.getId());

        return  entity;
    }
}
