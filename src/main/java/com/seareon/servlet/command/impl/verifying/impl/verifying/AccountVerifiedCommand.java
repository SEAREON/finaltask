package com.seareon.servlet.command.impl.verifying.impl.verifying;

import com.seareon.entity.impl.Account;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.verifying.VerifyCommand;
import oracle.jrockit.jfr.tools.ConCatRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class implements the Account verification command.
 * @version 1.0
 * @author Misha Ro
 */
public class AccountVerifiedCommand extends VerifyCommand<Account> {
    /**
     * The method marks Account as verified.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

        verify(request, Constant.ACCOUNT_DAO);
    }
}
