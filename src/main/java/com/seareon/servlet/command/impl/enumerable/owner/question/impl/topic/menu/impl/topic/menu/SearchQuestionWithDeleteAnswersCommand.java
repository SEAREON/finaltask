package com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.impl.topic.menu;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.QuestionQAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.impl.enumerable.owner.question.impl.topic.menu.TopicMenu;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The class implements the command to view all questions with delete answers.
 * @version 1.0
 * @author Misha Ro
 */
public class SearchQuestionWithDeleteAnswersCommand extends TopicMenu {
    /**
     * The method finds all questions with delete answers for this page and places them in the session attribute.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

        int countPages = (int) Math.ceil(
                ((QuestionQAO)abstractDAO).countQuestionsWithDeleteAnswers() / Constant.DOUBLE_TEN);
        int currentPage = Integer.parseInt(request.getParameter(Constant.CURRENT_PAGE));
        List<Question> questions =
                ((QuestionQAO)abstractDAO).findQuestionsWithDeleteAnswers(Constant.TEN * (currentPage - Constant.ONE),
                        currentPage * Constant.TEN);

        setTopicMenu(request, questions);
        setPagesCounter(request, currentPage, countPages);

        setCurrentPage(request, Constant.QUESTION_ANSWER_ADMIN_PAGE);
    }
}
