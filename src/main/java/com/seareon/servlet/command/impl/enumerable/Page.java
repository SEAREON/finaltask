package com.seareon.servlet.command.impl.enumerable;

import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;

import javax.servlet.http.HttpServletRequest;

/**
 * Abstract class for working with pages.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class Page extends ActionCommand {
    private final String CONTROLLER_COMMAND = "/controller?command=";
    private final String CONTROLLER_CURRENT_PAGE = "&currentPage=";

    /**
     *
     * @param request - HttpServletRequest object.
     * @param currentPage - Current page number.
     * @param countPages - Number of pages.
     */
    protected void setPagesCounter(HttpServletRequest request, int currentPage, int countPages) {
        request.getSession().setAttribute(Constant.CURRENT_PAGE_NUMBER, currentPage);
        request.getSession().setAttribute(Constant.COUNT_PAGES, countPages);
    }

    protected void setPageCommand(HttpServletRequest request) {
        String pageCommand =  CONTROLLER_COMMAND + request.getParameter(Constant.COMMAND) + CONTROLLER_CURRENT_PAGE;
        request.getSession().setAttribute(Constant.PAGE_COMMAND, pageCommand);
    }
}
