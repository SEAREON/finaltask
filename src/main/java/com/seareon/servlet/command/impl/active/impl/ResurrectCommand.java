package com.seareon.servlet.command.impl.active.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.Entity;
import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import com.seareon.servlet.command.impl.active.ActiveCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Misha Ro on 29.04.2017.
 */
public class ResurrectCommand extends ActiveCommand {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String daoType = request.getParameter(Constant.ENTITY);
        int id = Integer.parseInt(request.getParameter(Constant.ID));

        if(!daoType.equals(Constant.ANSWER) || checkQuestionActive(id)) {
            setActive(daoType, id);

            doOperationWithEntity(request, id, daoType);
        } else {
            request.setAttribute(Constant.ERROR_MESSAGE, Constant.CANNOT_RESTORE_ANSWER);
        }
    }

    @Override
    protected void setActive(String daoType, int id) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(daoType + Constant.DAO);
        abstractDAO.reestablish(id);
    }

    @Override
    protected void setQuestionPropertyWhenOperationWithAnswer(HttpServletRequest request, Answer answer) {
        Question question = (Question) request.getSession().getAttribute(Constant.OBJECT);

        question.setCountAnswers(question.getCountAnswers() + Constant.ONE);
        question.setDeleteAnswers(question.getDeleteAnswers() - Constant.ONE);

        if(answer.isNewEntity()) {
            question.setNewAnswers(question.getNewAnswers() + Constant.ONE);
        }

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        abstractDAO.update(question, question.getId());
    }

    @Override
    protected void setQuestionPropertyWhenOperationWithQuestion(HttpServletRequest request, List<Answer> answers) {
        Question question = (Question) request.getSession().getAttribute(Constant.OBJECT);

        question.setActive(true);
        question.setDeleteAnswers(Constant.ZERO);
        question.setCountAnswers(answers.size());

        for(Answer a : answers) {
            if(a.isNewEntity()) {
                question.setNewAnswers(question.getNewAnswers() + Constant.ONE);
            }
        }

        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);
        abstractDAO.update(question, question.getId());
    }

    @Override
    protected void setAccountActive(HttpServletRequest request) { }

    private boolean checkQuestionActive(int answerId) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance(Constant.ANSWER_DAO);
        Answer answer = (Answer) abstractDAO.findEntityById(answerId);

        abstractDAO = AbstractDAO.getInstance(Constant.QUESTION_DAO);

        if(((Question)abstractDAO.findEntityById(answer.getQuestionId())).isActive()) {
            return  true;
        }

        return false;
    }
}
