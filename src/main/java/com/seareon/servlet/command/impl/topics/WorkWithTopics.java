package com.seareon.servlet.command.impl.topics;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Topic;
import com.seareon.entity.impl.TopicQuestion;
import com.seareon.resource.ConfigurationManager;
import com.seareon.resource.Constant;
import com.seareon.servlet.command.ActionCommand;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The abstract class implements the command to create new themes.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class WorkWithTopics extends ActionCommand {
    /**
     * The method creates new themes.
     * @param topics - Array of topics.
     * @param question_id - Question id.
     * @param request - HttpServletRequest object.
     */
    protected void createTopics(String [] topics, int question_id, HttpServletRequest request) {

        AbstractDAO abstractTopicQuestionDAO = AbstractDAO.getInstance(Constant.TOPIC_QUESTION_DAO);
        AbstractDAO abstractTopicDAO = AbstractDAO.getInstance(Constant.TOPIC_DAO);
        Topic topicEntity = new Topic();
        List<Topic> topicList = (List<Topic>)request.getSession().getAttribute(Constant.TOPICS);
        TopicQuestion topicQuestion;
        Map<Integer, Integer> id;

        for(String topic : topics) {
            if (!topic.isEmpty()) {
                topicQuestion = new TopicQuestion();
                id = new HashMap<>(Constant.ONE);

                topicEntity.setText(topic);

                if (!abstractTopicDAO.isContainEntity(topicEntity)) {
                    int topicId = (int) abstractTopicDAO.create(topicEntity);
                    topicEntity.setId(topicId);
                }

                topicList.add(topicEntity);

                id.put(question_id, topicEntity.getId());
                topicQuestion.setId(id);

                abstractTopicQuestionDAO.create(topicQuestion);
            }
        }
    }
}
