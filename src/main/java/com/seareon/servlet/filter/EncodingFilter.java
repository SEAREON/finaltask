package com.seareon.servlet.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

/**
 * A class is a filter for tracking the encoding of requests and servlet responses.
 * @version 1.0
 * @author Misha Ro
 */
@WebFilter(urlPatterns = { "/*" },
        initParams = {@WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param") })
public class EncodingFilter implements Filter {
            private static final String ENCODING = "encoding";
    /**
     * Encoding value.
      */
    private String code;

    /**
     * The method initializes the filter.
     * @param filterConfig - FilterConfig object.
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        code = filterConfig.getInitParameter(ENCODING);
    }

    /**
     * The method sets the encoding if it is not set.
     * @param servletRequest - HttpServletRequest object.
     * @param servletResponse - HttpServletResponse object.
     * @param filterChain - FilterChain.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        String codeRequest = servletRequest.getCharacterEncoding();

        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            servletRequest.setCharacterEncoding(code);
            servletResponse.setCharacterEncoding(code);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() { }
}
