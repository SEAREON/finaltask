package com.seareon.servlet;

import com.seareon.resource.Constant;
import com.seareon.resource.MessageManager;
import com.seareon.resource.Util;
import com.seareon.servlet.command.ActionCommand;
import com.seareon.servlet.command.ActionFactory;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * The class implements the servlet of the application.
 * @version 1.0
 * @author Misha Ro
 */
@WebServlet("/controller")
@MultipartConfig
public class Controller extends HttpServlet {
    private static final String APP_MESS = "application/messages";
    private static final String ERROR_CODE = "error.code.";
    private static final String ERROR_CODE_LOGGER = " --- error.code ";
    private static final String RESPONSE_200 = " --- response.code 200\n\r\n\r";
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     *  Override {@link HttpServlet#doGet(HttpServletRequest, HttpServletResponse)}
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     *  Override {@link HttpServlet#doPost(HttpServletRequest, HttpServletResponse)}
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * The method defines the command and executes it. Then go to the desired page.
     * @param request - HttpServletRequest object.
     * @param response - HttpServletResponse object.
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);

        try {
            command.execute(request, response);
        } catch (Exception e) {
            logger.error(request.getSession().getId() + Constant.STR_LOGGER + e.getMessage() + Constant.STR_LOGGER_TAB);
            throw e;
        }

        if(Util.isDigit((String) request.getSession().getAttribute(Constant.CURRENT_PAGE))) {

            request.setAttribute(Constant.ERROR_MESSAGE, MessageManager.getProperty(APP_MESS,
                    ERROR_CODE + request.getSession().getAttribute(Constant.CURRENT_PAGE)));

            logger.info(request.getSession().getId() + ERROR_CODE_LOGGER +
                    request.getSession().getAttribute(Constant.CURRENT_PAGE) + Constant.STR_LOGGER_TAB);

            response.sendError(Integer.parseInt((String) request.getSession().getAttribute(Constant.CURRENT_PAGE)));
        } else {

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(
                    (String) request.getSession().getAttribute(Constant.CURRENT_PAGE));

            logger.info(request.getSession().getId() + RESPONSE_200);

            dispatcher.forward(request, response);
        }
    }
}
