package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Answer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The class implements queries on the Answer table.
 * @version 1.0
 * @author Misha Ro
 */
public class AnswerDAO extends AbstractDAO<Integer,Answer> {
    /**
     * The query finds an entry from the Answer table by question id.
     */
    private final String SQL_SELECT_ANSWER_BY_QUESTION_ID = "select * from answer where Question_id = ?";

    /**
     * The query finds an entry from the Answer table by id.
     */
    private final String SQL_SELECT_ANSWER_BY_ID = "select * from answer where id = ?";

    /**
     * The query updates the entry in the Answer table.
     */
    private final String SQL_UPDATE = "update answer set text = ?, mark = ?, new = ?, active = ? where id = ?";

    /**
     * The request creates an Answer record.
     */
    private final String SQL_INSERT_ANSWER = "insert into answer (id, Account_id, Question_id, text, mark, time," +
            " new, active) values (null, ?, ?, ?, null, ?, null, null)";

    /**
     * The request deletes the entry from the Answer table by id.
     */
    private final String SQL_DELETE_ENTITY_BY_ID = "update answer set active = 0 where id = ?";

    /**
     * Query recovers a record from the Answer table by id.
     */
    private final String SQL_REESTABLISH_ENTITY_ID = "update answer set active = 1 where id = ?";

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    @Override
    public List<Answer> findEntitiesFromTo(int start, int end) {
        return null;
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Answer findEntityById(Integer id) {

        return (Answer) doPreparedStatement(SQL_SELECT_ANSWER_BY_ID, actionReadEntityFromDB, id);
    }

    /**
     * Searching for an entity by question id
     * @param id - Question id
     * @return -  - Entity found by question id
     */
    public List<Answer> findEntitiesByQuestionId(Integer id) {

        return (List<Answer>) doPreparedStatement(SQL_SELECT_ANSWER_BY_QUESTION_ID, actionReadEntitiesFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}.
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean isContainEntity(Answer entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}.
     * @return - The number of entities in the database.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public int countEntities() {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     */
    @Override
    public boolean delete(Integer id) {

        return (boolean) doPreparedStatement(SQL_DELETE_ENTITY_BY_ID, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Answer object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Answer entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     */
    @Override
    public boolean reestablish(Integer id) {

        return (boolean) doPreparedStatement(SQL_REESTABLISH_ENTITY_ID, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Integer create(Answer entity) {

        return (Integer) doPreparedStatement(SQL_INSERT_ANSWER, actionCreateEntityInDB, entity.getAccountId(),
                entity.getQuestionId(), entity.getText(), entity.getTime());
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     */
    @Override
    public Answer update(Answer entity, Integer id) {

        Answer oldAnswer = findEntityById(id);

        if((boolean) doPreparedStatement(SQL_UPDATE, actionUpdatedEntitiesInDB, entity.getText(), entity.getMark(),
                entity.isNewEntity(), entity.isActive(), id)) {

            return oldAnswer;
        } else {
            return null;
        }
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected Answer createEntity(ResultSet resultSet) throws SQLException {
        return new Answer(resultSet);
    }
}
