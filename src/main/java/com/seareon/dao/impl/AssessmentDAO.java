package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Assessment;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * The class implements queries on the Assessment table.
 * @version 1.0
 * @author Misha Ro
 */
public class AssessmentDAO extends AbstractDAO<Map<Integer, Integer>, Assessment> {
    /**
     * The query returns an entry from the Assessment table by Answer_id and Account_id
     */
    private final String SQL_SELECT_CONTAIN_ENTITY = "select count(*) from assessment where Answer_id = ? and " +
            "Account_id = ?";

    /**
     * The request creates an Assessment record.
     */
    private final String SQL_INSERT_ENTITY = "insert into assessment (Answer_id, Account_id) values (?, ?)";
/*    private final String SQL_SELECT_FIND_BY_ANSWER_ID = "select * from assessment where Answer_id = ?"; // is it necessary?
    private final String SQL_SELECT_FIND_BY_TOPIC_ID = "select * from topic_question where Topic_id = ?";   // is it necessary?*/

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    @Override
    public List<Assessment> findEntitiesFromTo(int start, int end) {
        return null;
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Assessment findEntityById(Map<Integer, Integer> id) {
        return null;
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}.
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     */
    @Override
    public boolean isContainEntity(Assessment entity) {

        int answerId = entity.getId().keySet().iterator().next();
        int accountId = entity.getId().get(answerId);

        return (boolean) doPreparedStatement(SQL_SELECT_CONTAIN_ENTITY, actionChecksContainedEntityInDB, answerId,
                accountId);
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}.
     * @return - The number of entities in the database.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public int countEntities() {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Map<Integer, Integer> id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Assessment entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean reestablish(Map<Integer, Integer> id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Map<Integer, Integer> create(Assessment entity) {

        int topic_id = entity.getId().keySet().iterator().next();
        int question_id = entity.getId().get(topic_id);

        if((boolean) doPreparedStatement(SQL_INSERT_ENTITY, actionUpdatedEntitiesInDB, topic_id, question_id)) {

            return entity.getId();
        }

        return null;
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public Assessment update(Assessment entity, Map<Integer, Integer> id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    protected Assessment createEntity(ResultSet resultSet) throws SQLException {
        return null;
    }
}
