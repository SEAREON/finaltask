package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Util;
import java.sql.*;
import java.util.*;

/**
 * The class implements queries on the Account table.
 * @version 1.0
 * @author Misha Ro
 */
public class AccountDAO extends AbstractDAO<Integer, Account>{
    /**
     * The request creates an Account record.
     */
    private final String SQL_INSERT_ACCOUNT = "insert into account (id, email, login, password, description, " +
            "image_id, rating, countOfQuestions, countOfAnswers, role, new, active) values (null, ?, ?, ?, ?, ?, " +
            "null, null, null, null, null, null)";

    /**
     * The query returns records from the Account table.
     */
    private final String SQL_SELECT_ACCOUNTS_FROM_TO = "select * from account where active = 1 order by rating desc " +
            "limit ?, ?";

    /**
     * The request deletes the entry from the Account table.
     */
    private final String SQL_SELECT_DELETE_ACCOUNTS_FROM_TO = "select * from account where active = 0 order" +
            " by rating desc limit ?, ?";

    /**
     * The query finds records from the Account table marked as new.
     */
    private final String SQL_SELECT_NEW_ACCOUNTS_FROM_TO = "select * from account where active = 1 and new = 1 order" +
            " by rating desc limit ?, ?";

    /**
     * The query finds an entry from the Account table by id.
     */
    private final String SQL_SELECT_ACCOUNT_BY_ID = "select * from account where id = ?";

    /**
     * The query returns the number of records from the Account table with specific login and password.
     */
    private final String SQL_SELECT_CONTAIN_ACCOUNT = "select count(*) from account where login = ? and " +
            "password = sha1(sha1(?)) and active != 0";

    /**
     * The query returns the number of entries from the Account table with specific login and passwords that are active.
     */
    private final String SQL_SELECT_CONTAIN_ACCOUNT_ALTERNATIVE = "select count(*) from account where login = ? and " +
            "password = ? and active != 0";

    /**
     * The query updates the entry in the Account table.
     */
    private final String SQL_UPDATE = "update account set email = ?, login = ?, image_id = ?, description = ?, " +
            "role = ?, new = ?, active = ? where id = ?";

    /**
     * The query returns the number of active entries in the Account table.
     */
    private final String SQL_SELECT_COUNT_ENTITIES = "select count(*) from account where active = 1";

    /**
     * The query returns the number of active entries in the Account table that are active.
     */
    private final String SQL_SELECT_COUNT_NEW_ENTITIES = "select count(*) from account where active = 1 and new = 1";

    /**
     * The query returns the number of active entries in the Account table that are deleted.
     */
    private final String SQL_SELECT_COUNT_DELETE_ENTITIES = "select count(*) from account where active = 0";

    /**
     * The query returns an active record from the Account table for a specific login and password.
     */
    private final String SQL_SELECT_ENTITY_BY_LOGIN_AND_PASSWORD = "select * from account where login = ? and " +
            "password = sha1(sha1(?)) and active != 0";

    /**
     * Alternative query returns an active record from the Account table for a specific login and password.
     */
    private final String SQL_SELECT_ENTITY_BY_LOGIN_AND_PASSWORD_ALTERNATIVE = "select * from account where " +
            "login = ? and password = ? and active != 0";

    /**
     * The request deletes the entry from the Account table by id.
     */
    private final String SQL_DELETE_ENTITY_BY_ID = "update account set active = 0 where id = ?";

    /**
     * Query recovers a record from the Account table by id.
     */
    private final String SQL_REESTABLISH_ENTITY_ID = "update account set active = 1 where id = ?";

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    @Override
    public List<Account> findEntitiesFromTo(int start, int end) {

        return (List<Account>) doPreparedStatement(SQL_SELECT_ACCOUNTS_FROM_TO, actionReadEntitiesFromDB, start, end);
    }

    /**
     * The method finds new entries in the Account table
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Account> findNewAccountFromTo(int start, int end) {

        return (List<Account>) doPreparedStatement(SQL_SELECT_NEW_ACCOUNTS_FROM_TO, actionReadEntitiesFromDB, start,
                end);
    }

    /**
     * The method finds deleted entries in the Account table
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Account> findDeleteAccountFromTo(int start, int end) {

        return (List<Account>) doPreparedStatement(SQL_SELECT_DELETE_ACCOUNTS_FROM_TO, actionReadEntitiesFromDB, start,
                end);
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Account findEntityById(Integer id) {
        return (Account) doPreparedStatement(SQL_SELECT_ACCOUNT_BY_ID, actionReadEntityFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     */
    @Override
    public boolean isContainEntity(Account entity) {

        if((boolean) doPreparedStatement(SQL_SELECT_CONTAIN_ACCOUNT, actionChecksContainedEntityInDB, entity.getLogin(),
                entity.getPassword())) {

            Account account = (Account) doPreparedStatement( SQL_SELECT_ENTITY_BY_LOGIN_AND_PASSWORD,
                    actionReadEntityFromDB, entity.getLogin(), entity.getPassword());
            Util.copyFieldsAccount(account, entity);

            return true;
        } else if((boolean) doPreparedStatement(SQL_SELECT_CONTAIN_ACCOUNT_ALTERNATIVE, actionChecksContainedEntityInDB,
                entity.getLogin(), entity.getPassword())) {

            Account account = (Account) doPreparedStatement( SQL_SELECT_ENTITY_BY_LOGIN_AND_PASSWORD_ALTERNATIVE,
                    actionReadEntityFromDB, entity.getLogin(), entity.getPassword());
            Util.copyFieldsAccount(account, entity);

            return true;
        } else  {
            return false;
        }
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}
     * @return - The number of entities in the database.
     */
    @Override
    public int countEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of new entities in the database.
     * @return - The number of the new entities in the database.
     */
    public int countNewEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_NEW_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of deleted entities in the database.
     * @return - The number of the deleted entities in the database.
     */
    public int countDeleteEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_DELETE_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     */
    @Override
    public boolean delete(Integer id) {

        return (boolean) doPreparedStatement(SQL_DELETE_ENTITY_BY_ID, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Account entity) {
        return false;
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     */
    @Override
    public boolean reestablish(Integer id) {

        return (boolean) doPreparedStatement(SQL_REESTABLISH_ENTITY_ID, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#create}
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Integer create(Account entity) {

        return (Integer) doPreparedStatement(SQL_INSERT_ACCOUNT, actionCreateEntityInDB, entity.getEmail(),
                entity.getLogin(), entity.getPassword(), entity.getDescription(),
                entity.getImage() == 0 ? null : entity.getImage());
    }

    /**
     * Overridden method {@link AbstractDAO#update}
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     */
    @Override
    public Account update(Account entity, Integer id) {

        Account oldAccount = findEntityById(id);

        if((boolean) doPreparedStatement(SQL_UPDATE, actionUpdatedEntitiesInDB, entity.getEmail(), entity.getLogin(),
                entity.getImage() == 0 ? null : entity.getImage(), entity.getDescription(), entity.isRole(),
                entity.isNewEntity(), entity.isActive(), id)) {

            return oldAccount;
        } else {
            return null;
        }
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected Account createEntity(ResultSet resultSet) throws SQLException {
        return new Account(resultSet);
    }
}
