package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.TopicQuestion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * The class implements queries on the TopicQuestion table.
 * @version 1.0
 * @author Misha Ro
 */
public class TopicQuestionDAO extends AbstractDAO<Map<Integer, Integer>, TopicQuestion> {
    /**
     * The query returns the number of records from the TopicQuestion table with specific topic_id and question_id.
     */
    private final String SQL_SELECT_CONTAIN_ENTITY = "select count(*) from topic where topic_id = ? and " +
            "question_id = ?";

    /**
     * The request creates an TopicQuestion record.
     */
    private final String SQL_INSERT_ENTITY = "insert into topic_question (question_id, topic_id) values (?, ?)";

    /**
     * The query finds an entry from the TopicQuestion table by Question_id.
     */
    private final String SQL_SELECT_FIND_BY_QUESTION_ID = "select * from topic_question where Question_id = ?";

    /**
     * The query finds an entry from the TopicQuestion table by Topic_id.
     */
    private final String SQL_SELECT_FIND_BY_TOPIC_ID = "select * from topic_question where Topic_id = ?";

    /**
     * The request deletes the entry from the TopicQuestion table by id
     */
    private final String SQL_DELETE_ENTITY = "delete from topic_question where Question_id = ? and Topic_id = ?";

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public List<TopicQuestion> findEntitiesFromTo(int start, int end) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public TopicQuestion findEntityById(Map<Integer, Integer> id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Searching for an entity by Question_id
     * @param id - Entity Question_id
     * @return - Entity found by Question_id
     */
    public List<TopicQuestion> findEntitiesByQuestionId(int id) {

        return (List<TopicQuestion>) doPreparedStatement(SQL_SELECT_FIND_BY_QUESTION_ID, actionReadEntitiesFromDB, id);
    }

    /**
     * Searching for an entity by Answer_id
     * @param id - Entity Answer_id
     * @return - Entity found by Answer_id
     */
    public List<TopicQuestion> findEntitiesByTopicId(int id) {

        return (List<TopicQuestion>) doPreparedStatement(SQL_SELECT_FIND_BY_TOPIC_ID, actionReadEntitiesFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     */
    @Override
    public boolean isContainEntity(TopicQuestion entity) {

        return (boolean) doPreparedStatement(SQL_SELECT_CONTAIN_ENTITY, actionChecksContainedEntityInDB,
                entity.getId());
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}
     * @return - The number of entities in the database.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public int countEntities() {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Map<Integer, Integer> id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     */
    @Override
    public boolean delete(TopicQuestion entity) {

        int questionId = entity.getId().keySet().iterator().next();
        int topicId = entity.getId().get(questionId);

        return (boolean) doPreparedStatement(SQL_DELETE_ENTITY, actionUpdatedEntitiesInDB, questionId, topicId);
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     */
    @Override
    public boolean reestablish(Map<Integer, Integer> id) {
        return false;
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Map<Integer, Integer> create(TopicQuestion entity) {

        int questionId = entity.getId().keySet().iterator().next();
        int topicId = entity.getId().get(questionId);


        if((boolean) doPreparedStatement(SQL_INSERT_ENTITY, actionUpdatedEntitiesInDB, questionId, topicId)) {

            return entity.getId();
        }
        return null;
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public TopicQuestion update(TopicQuestion entity, Map<Integer, Integer> id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected TopicQuestion createEntity(ResultSet resultSet) throws SQLException, IOException {
        return new TopicQuestion(resultSet);
    }
}
