package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Topic;
import com.seareon.resource.Util;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The class implements queries on the Topic table.
 * @version 1.0
 * @author Misha Ro
 */
public class TopicDAO extends AbstractDAO<Integer, Topic> {
    /**
     * The query returns records from the Topic table.
     */
    private final String SQL_SELECT_TOPIC_FROM_TO = "select * from topic where active = 1 limit ?, ?";

    /**
     * The query finds an entry from the Topic table by id.
     */
    private final String SQL_SELECT_TOPIC_BY_ID = "select * from topic where id = ?";

    /**
     * The request creates an Topic record.
     */
    private final String SQL_INSERT_TOPIC = "insert into topic (id, text, active) values (null, ?, null)";

    /**
     * * The query returns the number of entries in the Topic table.
     */
    private  final String SQL_SELECT_COUNT_ENTITIES = "select count(*) from topic";

    /**
     * The query returns the number of active entries from the Account table with specific text field
     */
    private final String SQL_SELECT_CONTAIN_TOPIC = "select count(*) from topic where text = ? and active != 0";

    /**
     * The query finds an entry from the Topic table by text field.
     */
    private final String SQL_SELECT_TOPIC_BY_TEXT = "select * from topic where text = ?";

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    @Override
    public List<Topic> findEntitiesFromTo(int start, int end) {

        return (List<Topic>) doPreparedStatement(SQL_SELECT_TOPIC_FROM_TO, actionReadEntitiesFromDB, start, end);
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Topic findEntityById(Integer id) {
        return (Topic) doPreparedStatement(SQL_SELECT_TOPIC_BY_ID, actionReadEntityFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     */
    @Override
    public boolean isContainEntity(Topic entity) {

        if((boolean) doPreparedStatement(SQL_SELECT_CONTAIN_TOPIC, actionChecksContainedEntityInDB, entity.getText())) {

            Topic tempTopic = (Topic) doPreparedStatement(SQL_SELECT_TOPIC_BY_TEXT, actionReadEntityFromDB,
                    entity.getText());

            Util.copyFieldsTopic(tempTopic, entity);

            return true;
        }

        return false;
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}
     * @return - The number of entities in the database.
     */
    @Override
    public int countEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Topic entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean reestablish(Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Integer create(Topic entity) {

        return (Integer) doPreparedStatement(SQL_INSERT_TOPIC, actionCreateEntityInDB, entity.getText());
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public Topic update(Topic entity, Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected Topic createEntity(ResultSet resultSet) throws SQLException, IOException {
        return new Topic(resultSet);
    }
}
