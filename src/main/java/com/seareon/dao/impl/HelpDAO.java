package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Help;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The class implements queries on the Help table.
 * @version 1.0
 * @author Misha Ro
 */
public class HelpDAO extends AbstractDAO<Integer, Help> {
    /**
     * The request creates an Help record.
     */
    private final String SQL_INSERT_HELP = "insert into help (id, title, text, account_id, time, new) " +
            "values (null, ?, ?, ?, ?, ?)";

    /**
     * The query returns records from the Help table.
     */
    private final String SQL_SELECT_FROM_TO = "select * from help where new = 0 limit ?, ?";

    /**
     * The request new the entry from the Help table.
     */
    private final String SQL_SELECT_NEW_HELP_FROM_TO = "select * from help where new = 1 limit ?, ?";

    /**
     * The query returns the number of entries in the Help table.
     */
    private final String SQL_SELECT_COUNT_ENTITIES = "select count(*) from help where new = 0";

    /**
     * The query returns the number of new entries in the Help table.
     */
    private final String SQL_SELECT_COUNT_NEW_ENTITIES = "select count(*) from help where new = 1";

    /**
     * The query finds an entry from the Help table by id.
     */
    private final String SQL_SELECT_ENTITY_BY_ID = "select * from help where id = ?";

    /**
     * The query updates the entry in the Help table.
     */
    private final String SQL_UPDATE = "update help set new = ? where id = ?";

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    @Override
    public List<Help> findEntitiesFromTo(int start, int end) {

        return (List<Help>) doPreparedStatement(SQL_SELECT_FROM_TO, actionReadEntitiesFromDB, start, end);
    }

    /**
     * The method finds new entries in the Help table
     * @param start
     * @param end
     * @return  - List of found entities
     */
    public List<Help> findNewEntitiesFromTo(int start, int end) {

        return (List<Help>) doPreparedStatement(SQL_SELECT_NEW_HELP_FROM_TO, actionReadEntitiesFromDB, start, end);
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Help findEntityById(Integer id) {

        return (Help) doPreparedStatement(SQL_SELECT_ENTITY_BY_ID, actionReadEntityFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}.
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean isContainEntity(Help entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}.
     * @return - The number of entities in the database.
     */
    @Override
    public int countEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of new entities in the database.
     * @return - The number of entities in the database.
     */
    public int countNewEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_NEW_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Help entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean reestablish(Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Integer create(Help entity) {

        return (Integer) doPreparedStatement(SQL_INSERT_HELP, actionCreateEntityInDB, entity.getTitle(), entity.getText(),
                entity.getAccountId(), entity.getTime(), entity.isNewEntity());
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     */
    @Override
    public Help update(Help entity, Integer id) {

        Help oldAccount = findEntityById(id);

        if((boolean) doPreparedStatement(SQL_UPDATE, actionUpdatedEntitiesInDB, entity.isNewEntity(), id)) {
            return oldAccount;
        } else {
            return null;
        }
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected Help createEntity(ResultSet resultSet) throws SQLException {
        return new Help(resultSet);
    }
}
