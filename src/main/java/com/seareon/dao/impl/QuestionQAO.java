package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.account.link.impl.Question;
import com.seareon.resource.Util;
import java.io.IOException;
import java.sql.*;
import java.util.List;

/**
 * The class implements queries on the Question table.
 * @version 1.0
 * @author Misha Ro
 */
public class QuestionQAO extends AbstractDAO<Integer, Question> {
    /**
     * The request creates an Question record.
     */
    private final String SQL_INSERT_QUESTION = "insert into question (id, Account_id, title, text, " +
            "time, new, new_answers, delete_answers, active, count_answers) values " +
            "(null, ?, ?, ?, ?, null, null, null, null, null)";

    /**
     * The query returns records from the Question table.
     */
    private final String SQL_SELECT_QUESTIONS_FROM_TO = "select * from question where active = 1 limit ?, ?";

    /**
     * The query returns new records from the Question table.
     */
    private final String SQL_SELECT_NEW_QUESTIONS_FROM_TO = "select * from question where active = 1 and new = 1 " +
            "limit ?, ?";

    /**
     * The query finds an entry from the Question table by id.
     */
    private final String SQL_SELECT_QUESTION_BY_ID = "select * from question where id = ?";

    /**
     * Request the number of records from the Question table for the given title
     */
    private final String SQL_SELECT_CONTAIN_QUESTION = "select count(*) from question where title = ?";

    /**
     * The query updates the entry in the Question table.
     */
    private final String SQL_UPDATE = "update question set title = ?, text = ?, count_answers = ?, new = ?, " +
            "new_answers = ?, delete_answers = ?,active = ? where id = ?";

    /**
     * The query returns the number of active entries in the Question table.
     */
    private final String SQL_SELECT_COUNT_ENTITIES = "select count(*) from question where active = 1";

    /**
     * The query returns the number of active entries in the Question table where new field equals 1.
     */
    private final String SQL_SELECT_COUNT_NEW_ENTITIES = "select count(*) from question where active = 1 and new = 1";

    /**
     * The query returns the number of deleted entries in the Question table.
     */
    private final String SQL_SELECT_COUNT_NOT_ACTIVE_ENTITIES = "select count(*) from question where active = 0";

    /**
     * The query returns the number of active entries in the Question table where new_answers field more than 0.
     */
    private final String SQL_SELECT_COUNT_ENTITIES_WITH_NEW_ANSWERS = "select count(*) from question where " +
            "active = 1 and new_answers > 0";

    /**
     * The query returns the number of active entries in the Question table where delete_answers field more than 0.
     */
    private final String SQL_SELECT_COUNT_ENTITIES_WITH_DELETE_ANSWERS = "select count(*) from question where " +
            "active = 1 and delete_answers > 0";

    /**
     *  The query returns the number of entries in the Question table where count_answers field equals 0.
     */
    private final String SQL_SELECT_COUNT_NOT_HAVE_ANSWER_ENTITIES = "select count(*) from question where " +
            "count_answers = 0";

    /**
     * The query finds an entry from the Question table by title.
     */
    private final String SQL_SELECT_QUESTION_BY_TITLE = "select * from question where title = ?";

    /**
     * The request deletes the entry from the Question table by id
     */
    private final String SQL_DELETE_ENTITY_BY_ID = "update question set active = 0 where id = ?";

    /**
     * Query recovers a record from the Question table by id.
     */
    private final String SQL_REESTABLISH_ENTIBY_ID = "update question set active = 1 where id = ?";

    /**
     * The query returns deleted records from the Question table.
     */
    private final String SQL_SELECT_FROM_TO_NOT_ACTIVE = "select * from question where active = 0 limit ?, ?";

    /**
     * The query returns records from the Question table where new_answers field more than 0.
     */
    private final String SQL_SELECT_FROM_TO_WITH_NEW_ANSWERS = "select * from question where active = 1 and " +
            "new_answers > 0 limit ?, ?";

    /**
     * The query returns records from the Question table where delete_answers field more than 0.
     */
    private final String SQL_SELECT_FROM_TO_WITH_DELETE_ANSWERS = "select * from question where active = 1 and " +
            "delete_answers > 0 limit ?, ?";

    /**
     * The query returns records from the Question table where count_answers field equals 0.
     */
    private final String SQL_SELECT_ENTITIES_WITHOUT_ANSWERS = "select * from question where active = 1 and " +
            "count_answers = 0 limit ?, ?";

    /**
     * The query returns records from the Question table by part title.
     */
    private final String SQL_SELECT_BY_PART_OF_TITLE = "select * from question where UPPER(title) like UPPER(?) and " +
            "active = 1";

    /**
     * The query finds records in the Question table where the count_answer field is 0.
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Question> findQuestionsWithoutAnswer(int start, int end) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_ENTITIES_WITHOUT_ANSWERS, actionReadEntitiesFromDB,
                start, end);
    }

    /**
     * The query finds records in the Question table where the new_answer field more than 0.
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Question> findQuestionsWithNewAnswers(int start, int end) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_FROM_TO_WITH_NEW_ANSWERS, actionReadEntitiesFromDB,
                start, end);
    }

    /**
     * The query finds records in the Question table where the delete_answer field is 0.
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Question> findQuestionsWithDeleteAnswers(int start, int end) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_FROM_TO_WITH_DELETE_ANSWERS, actionReadEntitiesFromDB,
                start, end);
    }

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    @Override
    public List<Question> findEntitiesFromTo(int start, int end) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_QUESTIONS_FROM_TO, actionReadEntitiesFromDB, start, end);
    }

    /**
     * Search for a specified number of new entities in the database.
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Question> findNewEntitiesFromTo(int start, int end) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_NEW_QUESTIONS_FROM_TO, actionReadEntitiesFromDB, start,
                end);
    }

    /**
     * Search for a specified number of deleted entities in the database.
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public List<Question> findDeleteEntitiesFromTo(int start, int end) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_FROM_TO_NOT_ACTIVE, actionReadEntitiesFromDB, start,
                end);
    }

    /**
     * Search for a specified number of entities in the database by part of title.
     * @param titlePart - Part title of entity
     * @return - List of found entities
     */
    public List<Question> findQuestionByPartOfTitle(String titlePart) {

        return (List<Question>) doPreparedStatement(SQL_SELECT_BY_PART_OF_TITLE, actionReadEntitiesFromDB, titlePart);
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Question findEntityById(Integer id) {
        return (Question) doPreparedStatement(SQL_SELECT_QUESTION_BY_ID, actionReadEntityFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}.
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     */
    @Override
    public boolean isContainEntity(Question entity) {

        if((boolean) doPreparedStatement(SQL_SELECT_CONTAIN_QUESTION, actionChecksContainedEntityInDB,
                entity.getTitle())) {

            Question tempQuestion = (Question) doPreparedStatement(SQL_SELECT_QUESTION_BY_TITLE, actionReadEntityFromDB,
                    entity.getTitle());

            Util.copyFieldsQuestion(tempQuestion, entity);

            return true;
        }

        return false;
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}.
     * @return - The number of entities in the database.
     */
    @Override
    public int countEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of new entities in the database.
     * @return - The number of new entities in the database.
     */
    public int countNewEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_NEW_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of delete entities in the database.
     * @return - The number of new entities in the database.
     */
    public int countNotActiveEntities() {

        return (int) doStatement(SQL_SELECT_COUNT_NOT_ACTIVE_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of entities in the database where count_answer field is 0.
     * @return - The number of new entities in the database.
     */
    public int countQuestionsWithoutAnyAnswers() {

        return (int) doStatement(SQL_SELECT_COUNT_NOT_HAVE_ANSWER_ENTITIES, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of entities in the database where new_answer field more than 0.
     * @return - The number of new entities in the database.
     */
    public int countQuestionsWithNewAnswers() {

        return (int) doStatement(SQL_SELECT_COUNT_ENTITIES_WITH_NEW_ANSWERS, actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * The method returns the number of entities in the database where deleted_answer field more than 0.
     * @return - The number of new entities in the database.
     */
    public int countQuestionsWithDeleteAnswers() {

        return (int) doStatement(SQL_SELECT_COUNT_ENTITIES_WITH_DELETE_ANSWERS,
                actionReturnAmountInclusionEntitiesInDB);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     */
    @Override
    public boolean delete(Integer id) {

        return (boolean) doPreparedStatement(SQL_DELETE_ENTITY_BY_ID, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Question entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     */
    @Override
    public boolean reestablish(Integer id) {

        return (boolean) doPreparedStatement(SQL_REESTABLISH_ENTIBY_ID, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Integer create(Question entity) {

        return (Integer) doPreparedStatement(SQL_INSERT_QUESTION, actionCreateEntityInDB, entity.getAccountId(),
                entity.getTitle(), entity.getText(), entity.getTime());
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     */
    @Override
    public Question update(Question entity, Integer id) {

        Question oldAccount = findEntityById(id);

        if((boolean) doPreparedStatement(SQL_UPDATE, actionUpdatedEntitiesInDB, entity.getTitle(),
                entity.getText(), entity.getCountAnswers(), entity.isNewEntity(), entity.getNewAnswers(),
                entity.getDeleteAnswers(), entity.isActive(), id)) {

            return oldAccount;
        } else {
            return null;
        }
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected Question createEntity(ResultSet resultSet) throws SQLException, IOException {
        return new Question(resultSet);
    }
}
