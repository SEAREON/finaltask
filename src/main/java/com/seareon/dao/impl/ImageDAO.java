package com.seareon.dao.impl;

import com.seareon.dao.AbstractDAO;
import com.seareon.entity.impl.Image;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The class implements queries on the Image table.
 * @version 1.0
 * @author Misha Ro
 */
public class ImageDAO extends AbstractDAO<Integer, Image> {
    /**
     * The query finds an entry from the Image table by id.
     */
    private final String SQL_SELECT_IMAGE_BY_ID = "select * from image where id = ?";

    /**
     * The request creates an Image record.
     */
    private final String SQL_INSERT_IMAGE = "insert into image (id, imageContent, type) values (null, ?, ?)";

    /**
     * The request deletes the entry from the Image table by id.
     */
    private final String SQL_DELETE_IMAGE = "delete from image where id = ?";

    /**
     * Overridden method {@link AbstractDAO#findEntitiesFromTo}
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public List<Image> findEntitiesFromTo(int start, int end) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#findEntityById}
     * @param id - Entity id
     * @return - Entity found by id
     */
    @Override
    public Image findEntityById(Integer id) {

        return (Image) doPreparedStatement(SQL_SELECT_IMAGE_BY_ID, actionReadEntityFromDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#isContainEntity}.
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean isContainEntity(Image entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#countEntities}.
     * @return - The number of entities in the database.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public int countEntities() {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     */
    @Override
    public boolean delete(Integer id) {

        return (boolean) doPreparedStatement(SQL_DELETE_IMAGE, actionUpdatedEntitiesInDB, id);
    }

    /**
     * Overridden method {@link AbstractDAO#delete}.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean delete(Image entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#reestablish}.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public boolean reestablish(Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#create}.
     * @param entity - Entity object.
     * @return - Object Id.
     */
    @Override
    public Integer create(Image entity) {

        return (Integer) doPreparedStatement(SQL_INSERT_IMAGE, actionCreateEntityInDB, entity.getImage(),
                entity.getType());
    }

    /**
     * Overridden method {@link AbstractDAO#update}.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     * @deprecated - A particular implementation does not support this method.
     */
    @Override
    public Image update(Image entity, Integer id) {
        throw new UnsupportedOperationException();
    }

    /**
     * Overridden method {@link AbstractDAO#createEntity}.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     */
    @Override
    protected Image createEntity(ResultSet resultSet) throws SQLException, IOException {
        return new Image(resultSet);
    }
}
