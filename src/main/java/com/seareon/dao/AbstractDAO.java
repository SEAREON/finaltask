package com.seareon.dao;

import com.seareon.dao.client.DaoEnum;
import com.seareon.entity.Entity;
import com.seareon.servlet.Controller;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The abstract class provides a common interface and implementation of some common methods and properties.
 * @param <K> - Defines the data type for Id.
 * @param <T> - Defines the data type for the entity for which the DAO level object is implemented.
 * @version 1.0
 * @author Misha Ro
 */
public abstract class AbstractDAO <K, T extends Entity> {
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * A functional interface provides an opportunity to implement sql-queries without parameters
     * @version 1.0
     * @author Misha Ro
     */
    protected interface Action {
        /**
         * The method makes a query to DB
         * @param st - The Statement Object
         * @param sqlQuery - The String Object which contains a request for DB
         * @return - Query result
         * @throws SQLException
         */
        Object doAction(Statement st, String sqlQuery) throws SQLException;
    }

    /**
     * A functional interface that provides an opportunity to implement sql-queries with parameters
     * @version 1.0
     * @author Misha Ro
     */
    protected interface PreparedAction {
        /**
         * The method makes a query with parameters to DB
         * @param st - The Statement Object
         * @return - Query result
         * @throws SQLException
         * @throws IOException
         */
        Object doPreparedAction(Statement st) throws SQLException, IOException;
    }

    /**
     * ConnectionPool object
     */
    private ConnectionPool connectionPool = ConnectionPool.getInstance();

    /**
     * Search for a specified number of entities in the database.
     * @param start - The entity number from which begins
     * @param end - The entity number of which ends the search
     * @return - List of found entities
     */
    public abstract List<T> findEntitiesFromTo(int start, int end);

    /**
     * Searching for an entity by id
     * @param id - Entity id
     * @return - Entity found by id
     */
    public abstract T findEntityById(K id);

    /**
     * The method checks that the entity is contained in the database
     * @param entity - Entity object
     * @return - If the entity is present in the database then true otherwise false
     */
    public abstract boolean isContainEntity(T entity);

    /**
     * The method returns the number of entities in the database.
     * @return - The number of entities in the database.
     */
    public abstract int countEntities();

    /**
     * The method deletes the entity by id from the database.
     * @param id - Object Id.
     * @return - If the entity is deleted then true otherwise false
     */
    public abstract boolean delete(K id);

    /**
     * The method deletes the entity by entity object the database.
     * @param entity - Entity object.
     * @return - If the entity is deleted then true otherwise false.
     */
    public abstract boolean delete(T entity);

    /**
     * The method retrieves the entity by id in the database.
     * @param id - Object Id.
     * @return - If the entity is restored then true otherwise false.
     */
    public abstract boolean reestablish(K id);

    /**
     * Creates an entity record in the database
     * @param entity - Entity object.
     * @return - Object Id.
     */
    public abstract K create(T entity);

    /**
     * The method updates the entity record in the database.
     * @param entity - Entity object.
     * @param id - Object Id.
     * @return - The entity before the update.
     */
    public abstract T update(T entity, K id);

    /**
     * The method creates a new entity from the ResultSet object.
     * @param resultSet - ResultSet object.
     * @return - Entity object.
     * @throws SQLException
     * @throws IOException
     */
    protected abstract T createEntity(ResultSet resultSet) throws SQLException, IOException;

    /**
     * Implementation of the PreparedAction interface. Executes the query to the database.
     * Based on the received ResultSet object creates a new entity and returns it.
     */
    protected PreparedAction actionReadEntityFromDB = new PreparedAction() {
        @Override
        public Object doPreparedAction(Statement st) throws SQLException {
            ResultSet resultSet = ((PreparedStatement)st).executeQuery();
            resultSet.next();
            try {
                return createEntity(resultSet);
            } catch (SQLException | IOException e) {
                logger.error(e.getMessage());
            }

            return null;
        }
    };

    /**
     * Implementation of the PreparedAction interface. Executes the query to the database.
     * Based on the received ResultSet, the object creates an entity list and returns it.
     */
    protected PreparedAction actionReadEntitiesFromDB = new PreparedAction() {
        @Override
        public Object doPreparedAction(Statement st) throws SQLException, IOException {
            List<T> list = new ArrayList<>();

            ResultSet resultSet = ((PreparedStatement)st).executeQuery();

            while(resultSet.next()) {
                T account = createEntity(resultSet);

                if(account != null) {
                    list.add(account);
                }
            }

            return list;
        }
    };

    /**
     * Implementation of the PreparedAction interface.
     * Executes the query to the database and checks whether the specified entity is contained in the database.
     */
    protected PreparedAction actionChecksContainedEntityInDB = new PreparedAction() {
        @Override
        public Object doPreparedAction(Statement st) throws SQLException {
            ResultSet resultSet = ((PreparedStatement)st).executeQuery();

            if(resultSet.next() && resultSet.getInt(1) == 1) {
                return true;
            }

            return false;
        }
    };

    /**
     * Implementation of the Action interface.
     * Executes the query to the database and returns the number of records.
     */
    protected Action actionReturnAmountInclusionEntitiesInDB = new Action() {
        @Override
        public Object doAction(Statement st, String sqlQuery) throws SQLException {
            ResultSet resultSet = st.executeQuery(sqlQuery);

            resultSet.next();
            return resultSet.getInt("count(*)");
        }
    };

    /**
     * Implementation of the PreparedAction interface. Runs a query to update the record to the database.
     * If one or more records are updated, then returns true otherwise false.
     */
    protected PreparedAction actionUpdatedEntitiesInDB = new PreparedAction() {
        @Override
        public Object doPreparedAction(Statement st) throws SQLException {
            if(((PreparedStatement) st).executeUpdate() > 0) {
                return true;
            }

            return false;
        }
    };

    /**
     * Implementation of the PreparedAction interface. Runs a query to update the record to the database.
     * If one or more records are updated, then returns the value of the first record field is different -1.
     */
    protected PreparedAction actionCreateEntityInDB = new PreparedAction() {
        @Override
        public Object doPreparedAction(Statement st) throws SQLException {
            if(((PreparedStatement) st).executeUpdate() > 0) {
                try (ResultSet generatedKeys = st.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                }
            }

            return -1;
        }
    };

    /**
     * The method returns a concrete implementation of the AbstractDAO class.
     * @param name - The name of the concrete implementation of the AbstractDAO class.
     * @return -  The concrete implementation of the AbstractDAO class.
     */
    public static AbstractDAO getInstance(String name) {
        try {
            DaoEnum daoEnum = DaoEnum.valueOf(name.toUpperCase());
            return daoEnum.getAbstractDAO();
        } catch(IllegalArgumentException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * The method executes the query to the database using a specific implementation of the Action interface.
     * @param sqlQuery - Query string.
     * @param action - The specific implementation of the Action interface.
     * @return - Query result.
     */
    protected Object doStatement(String sqlQuery, Action action) {
        Connection con = connectionPool.getConnection();

        try (Statement st = con.createStatement()) {
            return action.doAction(st, sqlQuery);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new IllegalArgumentException(e);
        } finally {
            connectionPool.freeConnection(con);
        }
    }

    /**
     * The method executes the query to the database using a specific implementation of the PreparedAction interface.
     * @param sqlQuery - Query string.
     * @param action - The specific implementation of the PreparedAction interface.
     * @param params - Query parameters.
     * @return - Query result.
     */
    protected Object doPreparedStatement(String sqlQuery, PreparedAction action, Object ... params) {
        Connection con = connectionPool.getConnection();

        try (PreparedStatement pSt = con.prepareStatement(sqlQuery,  Statement.RETURN_GENERATED_KEYS)) {
            for(int indexPrams = 0; indexPrams < params.length; indexPrams++) {
                setValueToPreparedStatement(pSt, indexPrams + 1, params[indexPrams]);
            }

            return action.doPreparedAction(pSt);
        } catch (SQLException | IOException e) {
            logger.error(e.getMessage());
            throw new IllegalArgumentException(e);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new IllegalArgumentException(e);
        } finally {
            connectionPool.freeConnection(con);
        }
    }

    /**
     * The method sets the values to a certain position of the PreparedStatement object.
     * @param pSt - PreparedStatement object.
     * @param index - The index position of the parameter in PreparedStatement object.
     * @param value - Parameter value.
     * @throws SQLException
     */
    protected void setValueToPreparedStatement(PreparedStatement pSt, int index, Object value) throws SQLException {
        if(value instanceof Integer) {
            pSt.setInt(index, (Integer) value);
        } else if(value instanceof Date) {
            pSt.setDate(index, new java.sql.Date(((Date) value).getTime()));
        } else if(value instanceof Boolean) {
            pSt.setBoolean(index, (Boolean) value);
        } else if(value instanceof byte[]) {
            pSt.setBytes(index, (byte[]) value);
        } else {
                pSt.setString(index, (String) value);
        }
    }
}
