package com.seareon.dao.client;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.impl.*;

/**
 * Enumeration for DAO level classes.
 * @version 1.0
 * @author Misha Ro
 */
public enum DaoEnum {
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.Account}
     */
    ACCOUNT_DAO {
        {
            this.abstractDAO = new AccountDAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.account.link.impl.Question}
     */
    QUESTION_DAO {
        {
            this.abstractDAO = new QuestionQAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.Image}
     */
    IMAGE_DAO {
        {
            this.abstractDAO = new ImageDAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.Topic}
     */
    TOPIC_DAO {
        {
            this.abstractDAO = new TopicDAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.TopicQuestion}
     */
    TOPIC_QUESTION_DAO {
        {
            this.abstractDAO = new TopicQuestionDAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.account.link.impl.Answer}
     */
    ANSWER_DAO {
        {
            this.abstractDAO = new AnswerDAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.Assessment}
     */
    ASSESSMENT_DAO {
        {
            this.abstractDAO = new AssessmentDAO();
        }
    },
    /**
     * DAO Encapsulation for {@link com.seareon.entity.impl.account.link.impl.Help}
     */
    HELP_DAO {
        {
            this.abstractDAO = new HelpDAO();
        }
    };

    protected AbstractDAO abstractDAO;

    public AbstractDAO getAbstractDAO() {
        return abstractDAO;
    }
}

