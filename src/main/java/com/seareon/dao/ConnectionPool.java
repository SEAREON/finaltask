package com.seareon.dao;

import com.seareon.resource.ConfigurationManager;
import com.seareon.resource.Constant;
import com.seareon.servlet.Controller;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The class implements the synchronized connection pool with the DB. Number of connections is limited.
 * @version 1.0
 * @author Misha Ro
 */
public class ConnectionPool {
    /**
     * Logger object
     */
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * The semaphore object determines the number of connections available
     */
    private static Semaphore semaphore;
    /**
     * The Lock object provides synchronization of access to methods/
     */
    private static Lock lock = new ReentrantLock();
    /**
     * DB host
     */
    private String host;
    /**
     * Login for accessing the DB
     */
    private String login;
    /**
     * Password  for accessing the DB
     */
    private String password;
    /**
     * DB connection queue
     */
    private static Queue<Connection> connections;
    /**
     * Instance ConnectionPool
     */
    private static ConnectionPool instance;

    /**
     * ConnectionPool constructor. Creates a new instance.
     */
    private ConnectionPool() {
        try {
            Map<String,String> map = ConfigurationManager.getProperties(Constant.JDBC_CONFIG, Constant.DB_LIMIT,
                    Constant.DB_HOST, Constant.DB_LOGIN, Constant.DB_PASSWORD);
            int poolSize = Integer.parseInt(map.get(Constant.DB_LIMIT));
            semaphore = new Semaphore(poolSize, true);
            connections = new LinkedList<>();
            host = map.get(Constant.DB_HOST);
            login = map.get(Constant.DB_LOGIN);
            password = map.get(Constant.DB_PASSWORD);
        } catch (FileNotFoundException e) {
            logger.error(Constant.FILE_NOT_FOUND + e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * The method checks to see if the ConnectionPool object is created. If so, it returns this object, if not, it
     * creates a new object and returns it.
     * @return - ConnectionPool instance.
     */
    public static ConnectionPool getInstance() {
        lock.lock();

        try {
            if(instance == null) {
                instance = new ConnectionPool();
            }
        } finally {
            lock.unlock();
        }

        return instance;
    }

    /**
     * If the limit is not exceeded, the method returns a Connection object
     * @return - Connection object
     */
    public Connection getConnection() {
        Connection connection = null;

        try {
            if(semaphore.tryAcquire(10, TimeUnit.SECONDS)) {
                lock.lock();

                try {
                    Class.forName(Constant.JDBC_DRIVER);
                    connection = connections.isEmpty() ? DriverManager.getConnection(host, login, password) :
                        connections.poll();
                } catch (SQLException e) {
                    logger.error(e.getMessage());
                } catch (ClassNotFoundException e) {
                    logger.error(e.getMessage());
                } finally {
                    lock.unlock();
                }
            }

            // connection exception?
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        return connection;
    }

    /**
     * The method returns the Connection object back to ConnectionPool.
     * @param connection - Connection object
     */
    public void freeConnection(Connection connection) {
        lock.lock();

        try {
            connections.add(connection);
            semaphore.release();
        } finally {
            lock.unlock();
        }
    }
}
