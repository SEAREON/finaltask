package com.seareon.entity.impl;

import com.seareon.entity.Entity;
import com.seareon.resource.Constant;
import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class describes avatar of account.
 * @version 1.0
 * @author Misha Ro
 */
public class Image extends Entity<Integer> {
    /**
     * Image in bytes.
     */
    private byte[] image;

    /**
     * Image type.
     */
    private String type;

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Image(ResultSet resultSet) throws SQLException, IOException {
        super(resultSet.getInt(Constant.ID));

        InputStream imageIS = resultSet.getBinaryStream(Constant.IMAGE_CONTENT);
        image = IOUtils.toByteArray(imageIS);
        type = resultSet.getString(Constant.TYPE);
    }

    /**
     * The constructor creates an Image object and fills in image and type fields.
     * @param inputStream - Input stream of bytes. Contains a picture.
     * @param imageName - Image name.
     * @throws IOException
     */
    public Image(InputStream inputStream, String imageName) throws IOException {
        image = IOUtils.toByteArray(inputStream);
        type = imageName.substring(imageName.length() - Constant.THREE, imageName.length());
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean isActive() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setActive(boolean active) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNewEntity() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setNewEntity(boolean newEntity) {
        throw new UnsupportedOperationException();
    }
}
