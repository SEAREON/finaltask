package com.seareon.entity.impl.account.link.impl;

import com.seareon.entity.impl.account.link.AccountLink;
import com.seareon.resource.Constant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * The class describes the question model.
 * @version 1.0
 * @author Misha Ro
 */
public class Question extends AccountLink implements Cloneable {
    /**
     * Question title.
     */
    private String title;

    /**
     * Question text.
     */
    private String text;

    /**
     * Date when the question was asked.
     */
    private Date time;

    /**
     * Number of answers to the question.
     */
    private int countAnswers;

    /**
     * Number of new answers to the question.
     */
    private int newAnswers;

    /**
     * Number of deleted answers to the question.
     */
    private int deleteAnswers;

    /**
     * Default constructor.
     */
    public Question() { }

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Question(ResultSet resultSet) throws SQLException {
        super(resultSet.getInt(Constant.ID), resultSet.getBoolean(Constant.NEW), resultSet.getBoolean(Constant.ACTIVE),
                resultSet.getInt(Constant.ACCOUNT_ID));
        title = resultSet.getString(Constant.TITLE);
        text = resultSet.getString(Constant.TEXT);
        time = resultSet.getDate(Constant.TIME);
        countAnswers = resultSet.getInt(Constant.COUNT_ANSWERS);
        newAnswers = resultSet.getInt(Constant.NEW_ANSWERS);
        deleteAnswers = resultSet.getInt(Constant.DELETE_ANSWERS);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getCountAnswers() {
        return countAnswers;
    }

    public void setCountAnswers(int countAnswers) {
        this.countAnswers = countAnswers;
    }

    public int getNewAnswers() {
        return newAnswers;
    }

    public void setNewAnswers(int newAnswers) {
        this.newAnswers = newAnswers;
    }

    public int getDeleteAnswers() {
        return deleteAnswers;
    }

    public void setDeleteAnswers(int deleteAnswer) {
        this.deleteAnswers = deleteAnswer;
    }

    @Override
    public int hashCode() {
        final int prime = Constant.THIRTY_ONE;
        int result = Constant.ONE;

        result = prime * result + getId().hashCode();
        result = prime * result + getAccountId();
        result = prime * result + title.hashCode();
        result = prime * result + text.hashCode();
        result = prime * result + time.hashCode();
        result = prime * result + countAnswers;
        result = prime * result + (isNewEntity() ? Constant.ONE : Constant.ZERO);
        result = prime * result + (isActive() ? Constant.ONE : Constant.ZERO);

        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Question questionOther = (Question) obj;

        if (!getId().equals(questionOther.getId()) || getAccountId() != questionOther.getAccountId() ||
                !title.equals(questionOther.getTitle()) || !text.equals(questionOther.getText()) ||
                !time.equals(questionOther.getTime()) || countAnswers != questionOther.getCountAnswers() ||
                isNewEntity() != questionOther.isNewEntity() || isActive() != questionOther.isActive()) {

            return false;
        }

        return true;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Question obj = (Question) super.clone();

        if(time != null) {
            obj.setTime((Date) time.clone());
        }

        return  obj;
    }
}
