package com.seareon.entity.impl.account.link.impl;

import com.seareon.entity.impl.account.link.AccountLink;
import com.seareon.resource.Constant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * The class describes the model of answers to questions.
 * @version 1.0
 * @author Misha Ro
 */
public class Answer extends AccountLink implements Comparable<Answer> {
    /**
     * Question id related question.
     */
    private int questionId;

    /**
     * Answer text.
     */
    private String text;

    /**
     * Answer mark.
     */
    private int mark;

    /**
     * Date when the answer was given.
     */
    private Date time;

    /**
     * Default constructor.
     */
    public Answer() {}

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Answer(ResultSet resultSet) throws SQLException {
        super(resultSet.getInt(Constant.ID), resultSet.getBoolean(Constant.NEW), resultSet.getBoolean(Constant.ACTIVE),
                resultSet.getInt(Constant.ACCOUNT_ID));
        questionId = resultSet.getInt(Constant.QUESTION_ID);
        text = resultSet.getString(Constant.TEXT);
        mark = resultSet.getShort(Constant.MARK);
        time = resultSet.getDate(Constant.TIME);
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public int compareTo(Answer o) {
        if(isActive() && !o.isActive()) {
            return Constant.MINUS_ONE;
        } else if (!isActive() && o.isActive()) {
            return Constant.ONE;
        } else if(time == o.getTime()) {
            return mark - o.getMark();
        } else {
            return time.compareTo(o.getTime());
        }
    }
}
