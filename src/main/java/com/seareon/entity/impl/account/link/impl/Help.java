package com.seareon.entity.impl.account.link.impl;

import com.seareon.entity.impl.account.link.AccountLink;
import com.seareon.resource.Constant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * The class describes the model of the help message.
 * @version 1.0
 * @author Misha Ro
 */
public class Help extends AccountLink {
    /**
     * Help message title.
     */
    private String title;

    /**
     * Help message text.
     */
    private String text;

    /**
     * Date when asked for help
     */
    private Date time;

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Help(ResultSet resultSet) throws SQLException {
        super();
        setId(resultSet.getInt(Constant.ID));
        title = resultSet.getString(Constant.TITLE);
        text = resultSet.getString(Constant.TEXT);
        setAccountId(resultSet.getInt(Constant.ACCOUNT_ID));
        time = resultSet.getDate(Constant.TIME);
        setNewEntity(resultSet.getBoolean(Constant.NEW));
    }

    /**
     * Default constructor.
     */
    public Help() { }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
