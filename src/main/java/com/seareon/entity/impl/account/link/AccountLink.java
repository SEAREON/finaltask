package com.seareon.entity.impl.account.link;

import com.seareon.entity.Entity;

/**
 * An abstract class describing entities that contain a reference to the Account.
 * @version 1.0
 * @author Misha Ro
 */
public class AccountLink extends Entity<Integer> {
    /**
     * Account Id that created this entity.
     */
    private int accountId;

    /**
     * The constructor creates a new entity and sets id, active, newEntity and accountId fields.
     * @param id - Entity id.
     * @param newEntity - Novelty of entity.
     * @param active - State of entity.
     * @param accountId - Account id.
     */
    public AccountLink(int id, Boolean newEntity, Boolean active, int accountId) {
        super(id, newEntity, active);
        this.accountId = accountId;
    }

    /**
     * The constructor creates a new entity and sets id, active and accountId fields.
     * @param id - Entity id.
     * @param active - State of entity.
     * @param accountId - Account id.
     */
    public AccountLink(int id, boolean active, int accountId) {
        super(id, active);
        this.accountId = accountId;
    }

    /**
     * The constructor creates a new entity and sets id and accountId fields.
     * @param id - Entity id.
     * @param accountId - Account id.
     */
    public AccountLink(int id, int accountId) {
        super(id);
        this.accountId = accountId;
    }

    /**
     * Default constructor.
     */
    public AccountLink() { }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
