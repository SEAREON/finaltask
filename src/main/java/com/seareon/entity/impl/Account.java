package com.seareon.entity.impl;

import com.seareon.entity.Entity;
import com.seareon.resource.Constant;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class describes account model.
 * @version 1.0
 * @author Misha Ro
 */
public class Account extends Entity<Integer> implements Cloneable {
    /**
     * Account email.
     */
    private String email;

    /**
     * Account login.
     */
    private String login;

    /**
     * Account password.
     */
    private String password;

    /**
     * Account description.
     */
    private String description;

    /**
     * Account image.
     */
    private int image;

    /**
     * Account rating.
     */
    private int rating;

    /**
     * Number of questions asked by the account.
     */
    private int countOfQuestions;

    /**
     * Number of answers by the account.
     */
    private int countOfAnswers;

    /**
     * Account role.
     */
    private boolean role;

    /**
     * Default constructor.
     */
    public Account() { }

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Account(ResultSet resultSet) throws SQLException {
        super(Integer.parseInt(resultSet.getString(Constant.ID)), resultSet.getBoolean(Constant.NEW),
                resultSet.getBoolean(Constant.ACTIVE));
        email = resultSet.getString(Constant.EMAIL);
        login = resultSet.getString(Constant.LOGIN);
        password = resultSet.getString(Constant.PASSWORD);
        description = resultSet.getString(Constant.DESCRIPTION);
        image = resultSet.getInt(Constant.IMAGE_ID);
        rating = resultSet.getInt(Constant.RATING);
        countOfQuestions = resultSet.getInt(Constant.COUNT_OF_QUESTIONS);
        countOfQuestions = resultSet.getInt(Constant.COUNT_OF_ANSWERS);
        role = resultSet.getBoolean(Constant.ROLE);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getCountOfQuestions() {
        return countOfQuestions;
    }

    public void setCountOfQuestions(int countOfQuestions) {
        this.countOfQuestions = countOfQuestions;
    }

    public int getCountOfAnswers() {
        return countOfAnswers;
    }

    public void setCountOfAnswers(int countOfAnswers) {
        this.countOfAnswers = countOfAnswers;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        final int prime = Constant.THIRTY_ONE;
        int result = Constant.ONE;

        result = prime * result + getEmail().hashCode();
        result = prime * result + getLogin().hashCode();
        result = prime * result + getPassword().hashCode();
        result = prime * result + getDescription().hashCode();
        result = prime * result + image;
        result = prime * result + rating;
        result = prime * result + countOfQuestions;
        result = prime * result + countOfAnswers;
        result = prime * result + (role ? Constant.ONE : Constant.ZERO);
        result = prime * result + (isNewEntity() ? Constant.ONE : Constant.ZERO);
        result = prime * result + (isActive() ? Constant.ONE : Constant.ZERO);

        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Account accountOther = (Account) obj;

        if (!email.equals(accountOther.getEmail()) || !login.equals(accountOther.getLogin()) ||
                !description.equals(accountOther.getDescription()) ||
                image != accountOther.getImage() || rating != accountOther.getRating() ||
                countOfQuestions != accountOther.getCountOfQuestions() ||
                countOfAnswers != accountOther.getCountOfAnswers() || role != accountOther.isRole() ||
                isNewEntity() != accountOther.isNewEntity() || isActive() != accountOther.isActive()) {

            return false;
        }

        return true;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
