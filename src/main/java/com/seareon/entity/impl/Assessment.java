package com.seareon.entity.impl;

import com.seareon.entity.Entity;
import com.seareon.resource.Constant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * The class describes the relationship between the answer and rhe account.
 * @version 1.0
 * @author Misha Ro
 */
public class Assessment extends Entity<Map<Integer, Integer>> {
    /**
     * Default constructor.
     */
    public Assessment() {}

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Assessment(ResultSet resultSet) throws SQLException {
        Map<Integer, Integer> id = new HashMap<>(Constant.ONE);

        id.put(resultSet.getInt(Constant.ANSWER_ID), resultSet.getInt(Constant.ACCOUNT_ID));

        setId(id);
    }

    @Override
    public boolean isActive() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setActive(boolean active) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNewEntity() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setNewEntity(boolean newEntity) {
        throw new UnsupportedOperationException();
    }
}
