package com.seareon.entity.impl;

import com.seareon.entity.Entity;
import com.seareon.resource.Constant;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class describes topics of questions.
 * @version 1.0
 * @author Misha Ro
 */
public class Topic extends Entity<Integer> implements Cloneable {
    /**
     * Title of question.
     */
    private String text;

    /**
     * Number of questions belong to this topic
     */
    private int countQuestion;

    /**
     * Default constructor.
     */
    public Topic() {}

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Topic(ResultSet resultSet) throws SQLException {
        super(resultSet.getInt(Constant.ID), resultSet.getBoolean(Constant.ACTIVE));
        text = resultSet.getString(Constant.TEXT);
        countQuestion = resultSet.getInt(Constant.COUNT_QUESTIONS);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCountQuestion() {
        return countQuestion;
    }

    public void setCountQuestion(int countQuestion) {
        this.countQuestion = countQuestion;
    }

    @Override
    public void setNewEntity(boolean newEntity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNewEntity() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int hashCode() {
        final int prime = Constant.THIRTY_ONE;
        int result = Constant.ONE;

        result = prime * result + getId().hashCode();
        result = prime * result + text.hashCode();
        result = prime * result + countQuestion;
        result = prime * result + (isActive() ? Constant.ONE : Constant.ZERO);

        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Topic topicOther = (Topic) obj;

        if (!getId().equals(topicOther.getId()) ||
                !text.equals(topicOther.getText()) ||
                countQuestion != topicOther.getCountQuestion() ||
                isActive() != topicOther.isActive()) {

            return false;
        }

        return true;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
