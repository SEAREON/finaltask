package com.seareon.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A class describes an abstract model.
 * @param <T> - Defines the data type for Id.
 * @version 1.0
 * @author Misha Ro
 */
public class Entity<T> {
    /**
     * Entity ID.
     */
    private T id;

    /**
     * The field determines the state of the entity.
     */
    private boolean active;

    /**
     * The field determines whether the entity is new.
     */
    private boolean newEntity;

    /**
     * The constructor creates a new entity and sets id, active and newEntity fields.
     * @param id - Entity id.
     * @param newEntity - Novelty of entity.
     * @param active - State of entity.
     */
    public Entity(T id, Boolean newEntity, Boolean active) {
        this.id = id;
        this.active = active;
        this.newEntity = newEntity;
    }

    /**
     * The constructor creates a new entity and sets id and active fields.
     * @param id - Entity id.
     * @param active - State of entity.
     */
    public Entity(T id, boolean active) {
        this.id = id;
        this.active = active;
    }

    /**
     * The constructor creates a new entity and sets id field.
     * @param id - Entity id.
     */
    public Entity(T id) {
        this.id = id;
    }

    /**
     * Default constructor.
     */
    public Entity() { }

    /**
     * The constructor builds an object based on the query result to the database.
     * @param resultSet - Query result to the database.
     * @throws SQLException
     */
    public Entity(ResultSet resultSet) throws SQLException { }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isNewEntity() {
        return newEntity;
    }

    public void setNewEntity(boolean newEntity) {
        this.newEntity = newEntity;
    }
}
