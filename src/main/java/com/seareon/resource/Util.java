package com.seareon.resource;

import com.seareon.entity.impl.Account;
import com.seareon.entity.impl.Topic;
import com.seareon.entity.impl.account.link.impl.Answer;
import com.seareon.entity.impl.account.link.impl.Question;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class provides static utility methods.
 * @version 2.0
 * @author Misha Ro
 */
public class Util {
    /**
     * The method checks is a string with a digit
     * @param str - Check string.
     * @return - If the string contains a digit, then it returns true otherwise false
     */
    public static boolean isDigit (String str) {
        for (char ch : str.toCharArray()) {
            if (!Character.isDigit(ch)) {
                return false;
            }
        }

        return true;
    }

    /**
     * The method copies fields from one object to another.
     * @param from - The object from where the fields are copied
     * @param to - The object to which the fields are copied.
     */
    public static void copyFieldsAccount(Account from, Account to) {
        to.setId(from.getId());
        to.setEmail(from.getEmail());
        to.setDescription(from.getDescription());
        to.setImage(from.getImage());
        to.setRating(from.getRating());
        to.setCountOfQuestions(from.getCountOfQuestions());
        to.setCountOfAnswers(from.getCountOfAnswers());
        to.setRole(from.isRole());
        to.setNewEntity(from.isNewEntity());
        to.setActive(from.isActive());
    }

    /**
     * The method copies fields from one object to another.
     * @param from - The object from where the fields are copied
     * @param to - The object to which the fields are copied.
     */
    public static void copyFieldsQuestion(Question from, Question to) {
        to.setId(from.getId());
        to.setTitle(from.getTitle());
        to.setText(from.getText());
        to.setTime(from.getTime());
        to.setNewEntity(from.isNewEntity());
        to.setActive(from.isActive());
    }

    /**
     * The method copies fields from one object to another.
     * @param from - The object from where the fields are copied
     * @param to - The object to which the fields are copied.
     */
    public static void copyFieldsTopic(Topic from, Topic to) {
        to.setId(from.getId());
        to.setCountQuestion(from.getCountQuestion());
        to.setActive(from.isActive());
    }

    /**
     * Метод объединяет некоторое количество строк в одну строку.
     * @param strs - Array of strings.
     * @return - Concatenating all rows.
     */
    public static String getAnswer(String ... strs) {
        StringBuilder sb = new StringBuilder();

        for (String str : strs) {
            sb.append(str);
        }

        return sb.toString();
    }

    /**
     * The method checks a string for a match with a regular expression.
     * @param expression - Original string.
     * @param regexp - Regular expression.
     * @return - If the search sequence was found then true otherwise false.
     */
    public static boolean match(String expression,  String regexp) {
        Pattern p = Pattern.compile(regexp);
        Matcher m = p.matcher(expression);
        return m.matches();
    }
}
