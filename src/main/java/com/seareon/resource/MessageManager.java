package com.seareon.resource;

import com.seareon.servlet.Controller;
import org.apache.log4j.Logger;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * The class provides access to properties files for internationalization.
 * @version 2.0
 * @author Misha Ro
 */
public class MessageManager {
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * ResourceBundle object
     */
    private ResourceBundle resourceBundle;

    /**
     * The constructor creates a new object and initializes the ResourceBundle object.
     * @param fileName - Path to the file
     */
    private MessageManager(String fileName) {
        try {
            resourceBundle = ResourceBundle.getBundle(fileName, Locale.getDefault());
        }catch (MissingResourceException e) {
            logger.error(Constant.ERROR_CREATING_RESOURCE_BUNDLE);
        }
    }

    /**
     * The method finds the required resource and returns the value required by the key.
     * @param fileName - Path to the file
     * @param key - Label name.
     * @return - Label value.
     */
    public static String getProperty(String fileName, String key) {
        MessageManager mm = new MessageManager(fileName);
        return mm.resourceBundle.getString(key);
    }
}
