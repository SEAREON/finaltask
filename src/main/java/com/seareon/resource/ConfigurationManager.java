package com.seareon.resource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * The class provides access to configuration files.
 * @version 1.0
 * @author Misha Ro
 */
public class ConfigurationManager {
    /**
     * Properties object.
     */
    private static Properties properties = new Properties();

    /**
     * The method returns a key value pair from the specified file to all keys.
     * @param path - Path to the file
     * @param keys - The list of keys for which you want to get the value.
     * @return - Returns the key-value mapping.
     * @throws IOException
     */
    public static Map<String, String> getProperties(String path, String ... keys) throws IOException {

        Map<String, String> map = new HashMap<>();
        properties.load(new InputStreamReader(ConfigurationManager.class.getResourceAsStream(path)));

        for(String key : keys) {
            map.put(key, properties.getProperty(key));
        }

        return map;
    }
}
