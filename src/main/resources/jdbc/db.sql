-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema LikeIT
-- -----------------------------------------------------
-- The database describes the IT-Forum.

-- -----------------------------------------------------
-- Schema LikeIT
--
-- The database describes the IT-Forum.
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `LikeIT` DEFAULT CHARACTER SET utf8 ;
USE `LikeIT` ;

-- -----------------------------------------------------
-- Table `LikeIT`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `imageContent` MEDIUMBLOB NOT NULL,
  `type` CHAR(3) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`Account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`Account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `image_id` INT NULL,
  `email` VARCHAR(64) NOT NULL COMMENT 'The field is the email of the account. If account is blocked then cannot be created new account with blocking email',
  `login` VARCHAR(45) NOT NULL COMMENT 'The field is the login of the account. It is not unique because if exists blocking accounts then can create an account with a blocked login',
  `password` CHAR(40) NOT NULL COMMENT 'The field is the password of the account. It is not unique because if exists blocking accounts then can create an account with a blocked password. Encryption with md5()',
  `description` VARCHAR(1000) NULL COMMENT 'The fileld is some information about owner of the account',
  `rating` INT NOT NULL DEFAULT 0 COMMENT 'The field is the rating of good answers on some questions',
  `countOfQuestions` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The field contains a counter account questions',
  `countOfAnswers` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'The field contains a counter account answers',
  `role` BIT(1) NOT NULL DEFAULT 0 COMMENT 'The field is define role account. Can be administrator(0) or user(1)',
  `new` BIT(1) NOT NULL DEFAULT 1 COMMENT 'The field indicates that the account has not been verified by the moderator or administrator',
  `active` BIT(1) NOT NULL DEFAULT 1 COMMENT 'The field, reflecting the account exists(1) now or not(0)',
  INDEX `fk_Account_image1_idx` (`image_id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Account_image1`
    FOREIGN KEY (`image_id`)
    REFERENCES `LikeIT`.`image` (`id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`Question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`Question` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Synthetic identifier',
  `Account_id` INT NOT NULL,
  `title` VARCHAR(150) NOT NULL,
  `text` VARCHAR(1000) NOT NULL COMMENT 'The field is a text of the question',
  `time` DATETIME NOT NULL COMMENT 'A time when the question was created',
  `count_answers` INT NOT NULL,
  `new` BIT(1) NOT NULL DEFAULT 1 COMMENT 'The field indicates that the issue has not been verified by the moderator or administrator',
  `new_answers` INT NOT NULL DEFAULT 1,
  `delete_answers` INT NOT NULL,
  `active` BIT(1) NOT NULL DEFAULT 1 COMMENT 'The field which reflecting the question exists(1) or doesn\'t exists(0) now',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `text_UNIQUE` (`text` ASC),
  UNIQUE INDEX `Title_UNIQUE` (`title` ASC),
  INDEX `fk_Question_Account1_idx` (`Account_id` ASC),
  CONSTRAINT `fk_Question_Account1`
    FOREIGN KEY (`Account_id`)
    REFERENCES `LikeIT`.`Account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`Answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`Answer` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Synthetic identifier',
  `Account_id` INT NOT NULL,
  `Question_id` INT NOT NULL COMMENT 'The field is the foreign key to the table Question',
  `text` VARCHAR(1000) NOT NULL COMMENT 'The field is a text of the answer',
  `mark` SMALLINT NOT NULL DEFAULT 0 COMMENT 'The field is the evaluation of usefulness the answer (counter voted accounts)',
  `time` DATETIME NOT NULL COMMENT 'A time when the answer was created',
  `new` BIT(1) NOT NULL DEFAULT 1 COMMENT 'Field indicates that the answer has not been verified by the moderator or administrator',
  `active` BIT(1) NOT NULL DEFAULT 1 COMMENT 'The field which reflecting the account exists(1) or doesn\'t exists(0) now',
  PRIMARY KEY (`id`),
  INDEX `fk_Answer_Question1_idx` (`Question_id` ASC),
  UNIQUE INDEX `text_UNIQUE` (`text` ASC),
  INDEX `fk_Answer_Account1_idx` (`Account_id` ASC),
  CONSTRAINT `fk_Answer_Question1`
    FOREIGN KEY (`Question_id`)
    REFERENCES `LikeIT`.`Question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Answer_Account1`
    FOREIGN KEY (`Account_id`)
    REFERENCES `LikeIT`.`Account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`Topic`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`Topic` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(45) NOT NULL,
  `count_questions` INT NOT NULL,
  `active` BIT(1) NOT NULL DEFAULT 1 COMMENT 'The field which reflecting the topic exists(1) or doesn\'t exists(0) now',
  UNIQUE INDEX `text_UNIQUE` (`text` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`Assessment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`Assessment` (
  `Answer_id` INT NOT NULL COMMENT 'Reference on table of Answer ',
  `Account_id` INT NOT NULL,
  INDEX `fk_Assessment_Answer1_idx` (`Answer_id` ASC),
  PRIMARY KEY (`Answer_id`, `Account_id`),
  INDEX `fk_Assessment_Account1_idx` (`Account_id` ASC),
  CONSTRAINT `fk_Assessment_Answer1`
    FOREIGN KEY (`Answer_id`)
    REFERENCES `LikeIT`.`Answer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Assessment_Account1`
    FOREIGN KEY (`Account_id`)
    REFERENCES `LikeIT`.`Account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`topic_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`topic_question` (
  `Question_id` INT NOT NULL,
  `Topic_id` INT NOT NULL,
  INDEX `fk_topic_question_Question1_idx` (`Question_id` ASC),
  PRIMARY KEY (`Question_id`, `Topic_id`),
  INDEX `fk_topic_question_Topic1_idx` (`Topic_id` ASC),
  CONSTRAINT `fk_topic_question_Question1`
    FOREIGN KEY (`Question_id`)
    REFERENCES `LikeIT`.`Question` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_topic_question_Topic1`
    FOREIGN KEY (`Topic_id`)
    REFERENCES `LikeIT`.`Topic` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `LikeIT`.`help`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `LikeIT`.`help` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `text` VARCHAR(1000) NOT NULL,
  `Account_id` INT NOT NULL,
  `time` DATETIME NOT NULL,
  `new` BIT(1) NOT NULL DEFAULT 1,
  INDEX `fk_help_Account1_idx` (`Account_id` ASC),
  UNIQUE INDEX `text_UNIQUE` (`text` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_help_Account1`
    FOREIGN KEY (`Account_id`)
    REFERENCES `LikeIT`.`Account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `LikeIT`;

DELIMITER $$
USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Account_BEFORE_INSERT` BEFORE INSERT ON `Account` FOR EACH ROW
BEGIN
	set new.password = sha1(sha1(new.password));
    set new.rating = 0;
    set new.countOfQuestions = 0;
    set new.countOfAnswers = 0;
    set new.role = 0;
    set new.new = 1;
    set new.active = 1;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Account_BEFORE_UPDATE` BEFORE UPDATE ON `Account` FOR EACH ROW
BEGIN
	if old.active > new.active and new.role = 1 and (select count(*) from Account where role = 1) = 1				
		then
			set new.active = 1;
	elseif old.password != new.password
		then
			set new.password = sha1(sha1(new.password));
	end if;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Question_BEFORE_INSERT` BEFORE INSERT ON `Question` FOR EACH ROW
BEGIN
	set new.count_answers = 0;
	set new.new = 1;
    set new.new_answers = 0;
    set new.delete_answers = 0;
    set new.active = 1;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Question_AFTER_INSERT` AFTER INSERT ON `Question` FOR EACH ROW
BEGIN
	update Account set countOfQuestions = countOfQuestions + 1 where id = New.Account_id;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Question_BEFORE_UPDATE` BEFORE UPDATE ON `Question` FOR EACH ROW
BEGIN
	if old.text <> new.text
		then
			set new.new = 1;
	end if;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Question_AFTER_UPDATE` AFTER UPDATE ON `Question` FOR EACH ROW
BEGIN
	if old.active > new.active 
		then 
            update Answer set Answer.active = 0 where Answer.Question_id = new.id;
            update Account set Account.countOfQuestions = countOfQuestions - 1 where Account.id = New.Account_id;
            update Topic set count_questions = count_questions - 1 where id in 
				(select Topic_id from topic_question where Question_id = new.id);
	elseif old.active < new.active 
		then
			update Answer set Answer.active = 1 where Answer.Question_id = new.id;
			update Account set Account.countOfQuestions = countOfQuestions + 1 where Account.id = New.Account_id;  
            update Topic set count_questions = count_questions + 1 where id in 
				(select Topic_id from topic_question where Question_id = new.id);
	end if;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Answer_BEFORE_INSERT` BEFORE INSERT ON `Answer` FOR EACH ROW
BEGIN
	set new.mark = 0;
    set new.new = 1;
    set new.active = 1;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Answer_AFTER_INSERT` AFTER INSERT ON `Answer` FOR EACH ROW
BEGIN
	update Account set countOfAnswers = countOfAnswers + 1 where id = New.Account_id;
    update Question set count_answers = count_answers + 1 where id = New.Question_id;
    update Question set new_answers = new_answers + 1 where id = New.Question_id;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Answer_BEFORE_UPDATE` BEFORE UPDATE ON `Answer` FOR EACH ROW
BEGIN
	if old.text <> new.text
		then
			set new.new = 1;
	end if;
    
    if old.new > New.new
		then
			update Question set new_answers = new_answers - 1 where id = New.Question_id;
	elseif old.new < New.new
		then
			update Question set new_answers = new_answers + 1 where id = New.Question_id;
	end if;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Answer_AFTER_UPDATE` AFTER UPDATE ON `Answer` FOR EACH ROW
BEGIN
	if old.active > new.active 
		then 
			update Account set rating = rating - new.mark where id = New.Account_id;
            update Account set countOfAnswers = countOfAnswers - 1 where id = New.Account_id;
	elseif old.active < new.active 
		then
			update Account set rating = rating + new.mark where id = New.Account_id;
			update Account set countOfAnswers = countOfAnswers + 1 where id = New.Account_id;
	elseif new.mark <> old.mark 
			then
				if old.mark = 0
					then
						update Account set rating = rating + new.mark where id = New.Account_id;
				elseif old.mark > new.mark
					then
						update Account set rating = rating - (old.mark - new.mark) where id = New.Account_id;
				elseif old.mark < new.mark	
					then
						update Account set rating = rating + (new.mark - old.mark) where id = New.Account_id;
				end if;
	end if;    
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Topic_BEFORE_INSERT` BEFORE INSERT ON `Topic` FOR EACH ROW
BEGIN
	set new.active = 1;
    set new.count_questions = 0;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`Topic_AFTER_UPDATE` AFTER UPDATE ON `Topic` FOR EACH ROW
BEGIN
	if old.active > new.active 
		then 
            update Question set active = 0 where id in (select Question_id from topic_question where Topic_id = new.id);
	elseif old.active < new.active 
		then
			update Question set active = 1 where id in (select Question_id from topic_question where Topic_id = new.id);    
	end if;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`topic_question_AFTER_INSERT` AFTER INSERT ON `topic_question` FOR EACH ROW
BEGIN
	update Topic set count_questions = count_questions + 1 where id = new.Topic_id;
END$$

USE `LikeIT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `LikeIT`.`topic_question_AFTER_DELETE` AFTER DELETE ON `topic_question` FOR EACH ROW
BEGIN
	update Topic set count_questions = count_questions - 1 where id = old.Topic_id;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
