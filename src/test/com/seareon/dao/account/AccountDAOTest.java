package com.seareon.dao.account;

import com.seareon.dao.AbstractDAO;
import com.seareon.dao.ConnectionPool;
import com.seareon.entity.Entity;
import com.seareon.entity.impl.Account;
import com.seareon.resource.Util;
import com.seareon.servlet.command.ActionCommand;
import com.seareon.servlet.command.ActionFactory;
import com.seareon.servlet.command.impl.WrongAction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Misha Ro on 14.03.2017.
 */
public class AccountDAOTest {
    private Account account;

    @Before
    public void setAccountRecord() throws Exception {
        account = new Account();
        account.setEmail("test@mail.ru");
        account.setLogin("test");
        account.setPassword("test666");

        Connection con = ConnectionPool.getInstance().getConnection();

        try {

            Statement st = con.createStatement();

            st.executeUpdate("insert into account (id, email, login, password, description, " +
                    "image_id, rating, countOfQuestions, countOfAnswers, role, new, active) values (null, " +
                    "'test@mail.ru', 'test', 'test666', '', null, null, null, null, null, null, null)");
        } finally {
            ConnectionPool.getInstance().freeConnection(con);
        }
    }

    @After
    public void deleteAccountRecord() throws Exception {
        Connection con = ConnectionPool.getInstance().getConnection();

        try {

            Statement st = con.createStatement();

            st.executeUpdate("delete from account where login = 'test'");
        } finally {
            ConnectionPool.getInstance().freeConnection(con);
        }
    }

    @Test
    public void isContainAccount() throws Exception {
        AbstractDAO abstractDAO = AbstractDAO.getInstance("account_dao");
        Account temp = new Account();

        temp.setLogin("test");
        temp.setPassword("test666");

        Assert.assertTrue(abstractDAO.isContainEntity(temp));
    }

    @Test
    public void deleteAccount() throws Exception {
        AbstractDAO abstractDAO = AbstractDAO.getInstance("account_dao");

        abstractDAO.isContainEntity(account);

        Assert.assertTrue(abstractDAO.delete(account.getId()));

        Account temp = (Account) abstractDAO.findEntityById(account.getId());

        Assert.assertFalse(temp.isActive());
    }

    @Test
    public void getAccount() throws Exception {
        AbstractDAO abstractDAO = AbstractDAO.getInstance("account_dao");
        abstractDAO.isContainEntity(account);

        Account temp = (Account) abstractDAO.findEntityById(account.getId());

        Assert.assertEquals(account, temp);
    }

    @Test
    public void getAccounts() throws Exception  {
        AbstractDAO abstractDAO = AbstractDAO.getInstance("account_dao");
        List<Entity> entities = abstractDAO.findEntitiesFromTo(1, 100);

        Assert.assertNotEquals(entities.size(), 0);
    }

    @Test
    public void updateAccount() throws Exception {
        AbstractDAO abstractDAO = AbstractDAO.getInstance("account_dao");
        abstractDAO.isContainEntity(account);

        account.setDescription("blabla");

        abstractDAO.update(account, account.getId());
        Account temp = (Account) abstractDAO.findEntityById(account.getId());

        Assert.assertEquals("blabla", temp.getDescription());
    }

    @Test
    public void countEntities() throws Exception {
        AbstractDAO abstractDAO = AbstractDAO.getInstance("account_dao");

        Assert.assertNotEquals(0, abstractDAO.countEntities());
    }

    @Test
    public void checkWrongCommand() {
        ActionFactory actionFactory  =new ActionFactory();

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getParameter("command")).thenReturn("blabla");

        ActionCommand actionCommand = actionFactory.defineCommand(request);

        Assert.assertTrue(actionCommand instanceof WrongAction);
    }

    @Test
    public void isDigit() {
        Assert.assertTrue(Util.isDigit("66"));
        Assert.assertFalse(Util.isDigit("66$"));
    }
}